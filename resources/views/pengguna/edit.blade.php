@extends('layouts.maindashboard')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <!-- Notifikasi menggunakan flash session data -->
                @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-error" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif

                <div class="card border-0 shadow rounded">
                <div class="card-header">
                     <h1 class="card-title"><strong>Form Ubah Pengguna</strong></h1>
                    </div>
                    <div class="card-body">
                    
                        <form action="{{ route('pengguna.update', $model->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="nama_pengguna">Nama Pengguna</label>
                                <input type="text" class="form-control @error('nama_pengguna') is-invalid @enderror"
                                    name="nama_pengguna" value="{{ old('nama_pengguna', $model->nama_pengguna) }}" required>

                                <!-- error message untuk title -->
                                @error('nama_pengguna')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-4">
                                <label for="username">Username</label>
                                <input type="text" class="form-control @error('username') is-invalid @enderror"
                                    name="username" value="{{ old('username', $model->username) }}" required>

                                <!-- error message untuk title -->
                                @error('username')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-4">
                                <label for="password">Password</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                    name="password" value="{{ old('password', $model->password) }}" disabled>

                                <!-- error message untuk title -->
                                @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-4">
                                <label for="email">Email</label>
                                <input type="text" class="form-control"
                                    name="email" value="{{ old('email', $model->email) }}">
                            </div>

                            <div class="form-group col-md-4">
                            <label for="handphone">No. Handphone</label>
                                <input type="text" class="form-control"
                                    name="handphone" value="{{ old('handphone', $model->handphone) }}" >
                            </div>

                            <div class="form-group col-md-4">
                            <label for="hak_akses">Hak Akses Pengguna</label>
                            <select id="hak_akses" class="form-control" name="hak_akses" value="{{ old('hak_akses', $model->hak_akses) }}" required>
                                <option hidden>Pilih Hak Akses</option>
                                <option value="Administrator" {{$model->hak_akses == 'Administrator' ? 'selected': '' }}>Administrator</option>
                                <option value="Dekan" {{$model->hak_akses == 'Dekan' ? 'selected': '' }}>Dekan</option>
                                <option value="Kepala Puskom" {{$model->hak_akses == 'Kepala Puskom' ? 'selected': '' }}>Kepala Puskom</option>
                                <option value="Kaprodi TI" {{$model->hak_akses == 'Kaprodi TI' ? 'selected': '' }}>Kaprodi TI</option>
                                <option value="Kaprodi SI" {{$model->hak_akses == 'Kaprodi SI' ? 'selected': '' }}>Kaprodi SI</option>
                            </select>
                            </div>

                            <div class="form-group col-md-8">
                            <label for="alamat">Alamat Tempat Tinggal</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" name="alamat" onKeyPress class="form-control" rows="3"> {{ old('alamat') }}</textarea>
                            </div>

                          

                            </div>
                            <button type="submit" class="btn btn-md btn-primary">Save</button>
                            <a href="{{ route('pengguna.index') }}" class="btn btn-md btn-secondary">Back</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <script>
        $(document).ready(function() {
  $("#success-alert").hide();
  $("#myWish").click(function showAlert() {
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
      $("#success-alert").slideUp(500);
    });
  });
});
    </script> -->

    <script>
// setTimeout( myStopFunction, 2500);

// function myStopFunction() {
//  $('#success-alert').hide();
// }

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
    </script>

@endsection