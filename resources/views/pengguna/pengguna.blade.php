@extends('layouts.maindashboard')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <!-- Notifikasi menggunakan flash session data -->

                @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-error" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif


                <div class="card border-0 shadow rounded">
                <div class="card-header">
                     <h1 class="card-title"><strong>Data Pengguna</strong></h1>
                </div>
                    <div class="card-body">
                        <a href="{{url('pengguna/create')}}" class="btn btn-md btn-success mb-3 float-right">Tambah Data</a>

                        <table id="role_table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Nama Pengguna</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">No. Handphone</th>
                                    <th scope="col">Hak Akses</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($datas as $data)
                                <tr>
                                    <td>{{ $data->nama_pengguna }}</td>
                                    <td>{{ $data->username }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ $data->handphone }}</td>
                                    <td>{{ $data->hak_akses }}</td>
                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                            action="{{ route('pengguna.destroy', $data->id) }}" method="POST">
                                            <a href="{{ route('pengguna.edit', $data->id) }}    "
                                                class="btn btn-sm btn-primary">Ubah</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center text-mute" colspan="6">Data post tidak tersedia</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


   

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script> -->

<script>
    $(document).ready(function() {
    $('#role_table').DataTable();
} );
</script>

<script>
// setTimeout( myStopFunction, 2500);

// function myStopFunction() {
//  $('#success-alert').hide();
// }

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
    </script>

    <!-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 -->
@endsection