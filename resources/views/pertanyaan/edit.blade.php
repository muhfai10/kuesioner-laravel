@extends('layouts.maindashboard')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <!-- Notifikasi menggunakan flash session data -->
                @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-error" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif

                <div class="card border-0 shadow rounded">
                <div class="card-header">
                     <h1 class="card-title"><strong>Form Ubah Pertanyaan</strong></h1>
                    </div>
                    <div class="card-body">
                    
                        <form action="{{ route('pertanyaan.update', $model->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-row">
                            
                            <div class="form-group col-md-4">
                                <label for="pertanyaan">Pertanyaan</label>
                                <input type="text" class="form-control @error('pertanyaan') is-invalid @enderror"
                                    name="pertanyaan" value="{{ old('pertanyaan', $model->pertanyaan) }}" required>

                                <!-- error message untuk title -->
                                @error('pertanyaan')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-4">
                                <label for="variabel">Variabel</label>
                                <input type="text" class="form-control"
                                    name="variabel" value="{{ old('variabel', $model->variabel) }}" >
                            </div>

                            <div class="form-group col-md-4">
                            <label for="id_dimensi">Dimensi</label>
                            <select id="id_dimensi" class="form-control @error('id_dimensi') is-invalid @enderror" name="id_dimensi" value="{{ old('id_dimensi', $model->id_dimensi ) }}" required>
                                <option hidden>Pilih Dimensi</option>
                                @foreach ($dimensis as $dimensi)
                                <option value="{{$dimensi->id}}" {{ $model->id_dimensi == $dimensi->id ? 'selected': '' }}>{{$dimensi->dimensi}}</option>
                                @endforeach
                                
                            </select>
                            <!-- error message untuk title -->
                            @error('id_dimensi')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>


                            <div class="form-group col-md-12">
                                <label for="pila">Pilihan A</label>
                                <input type="text" class="form-control @error('pila') is-invalid @enderror"
                                    name="pila" value="{{ old('pila', $model->pila) }}" required>

                                <!-- error message untuk title -->
                                @error('pila')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-12">
                                <label for="pilb">Pilihan B</label>
                                <input type="text" class="form-control @error('pilb') is-invalid @enderror"
                                    name="pilb" value="{{ old('pilb', $model->pilb) }}" required>

                                <!-- error message untuk title -->
                                @error('pilb')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-12">
                                <label for="pilc">Pilihan C</label>
                                <input type="text" class="form-control @error('pilc') is-invalid @enderror"
                                    name="pilc" value="{{ old('pilc', $model->pilc) }}" required>

                                <!-- error message untuk title -->
                                @error('pilc')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-12">
                                <label for="pild">Pilihan D</label>
                                <input type="text" class="form-control @error('pild') is-invalid @enderror"
                                    name="pild" value="{{ old('pild', $model->pild) }}" required>

                                <!-- error message untuk title -->
                                @error('pild')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-12">
                                <label for="pile">Pilihan E</label>
                                <input type="text" class="form-control @error('pile') is-invalid @enderror"
                                    name="pile" value="{{ old('pile', $model->pile) }}" required>

                                <!-- error message untuk title -->
                                @error('pile')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>





                           

                            </div>
                            <button type="submit" class="btn btn-md btn-primary">Save</button>
                            <a href="{{ route('pertanyaan.index') }}" class="btn btn-md btn-secondary">Back</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <script>
        $(document).ready(function() {
  $("#success-alert").hide();
  $("#myWish").click(function showAlert() {
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
      $("#success-alert").slideUp(500);
    });
  });
});
    </script> -->

    <script>
// setTimeout( myStopFunction, 2500);

// function myStopFunction() {
//  $('#success-alert').hide();
// }

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
    </script>

@endsection