@extends('layouts.maindashboard')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <!-- Notifikasi menggunakan flash session data -->
                @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-error" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif

                <div class="card border-0 shadow rounded">
                <div class="card-header">
                     <h1 class="card-title"><strong>Form Tambah Dimensi</strong></h1>
                    </div>
                    <div class="card-body">
                    
                        <form action="{{ route('dimensi.store') }}" method="POST">
                            @csrf
                            <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="dimensi">Nama Dimensi</label>
                                <input type="text" class="form-control @error('dimensi') is-invalid @enderror"
                                    name="dimensi" value="{{ old('dimensi') }}" required>

                                <!-- error message untuk title -->
                                @error('dimensi')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="bobot">Bobot Dimensi</label>
                                <input type="number" class="form-control @error('bobot') is-invalid @enderror"
                                    name="bobot" value="{{ old('bobot') }}" required>

                                <!-- error message untuk title -->
                                @error('bobot')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            </div>
                            <button type="submit" class="btn btn-md btn-primary">Save</button>
                            <a href="{{ route('dimensi.index') }}" class="btn btn-md btn-secondary">Back</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <script>
        $(document).ready(function() {
  $("#success-alert").hide();
  $("#myWish").click(function showAlert() {
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
      $("#success-alert").slideUp(500);
    });
  });
});
    </script> -->

    <script>
// setTimeout( myStopFunction, 2500);

// function myStopFunction() {
//  $('#success-alert').hide();
// }

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
    </script>

@endsection