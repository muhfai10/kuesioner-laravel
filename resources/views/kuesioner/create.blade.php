@extends('layouts.maindashboard')

@section('content')

<div class="container-fluid">
 <!-- Notifikasi menggunakan flash session data -->
                @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-error" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif

                <h1>Form Input Kuesioner</h1>

                @foreach ($datas as $key=> $data)

                <form action="{{ route('jawaban.store') }}" method="post" >
                @csrf
                <div class="card card-primary card-outline">
                <!-- <input type="hidden" class="form-control"
                                    name="id_kuesioner" value="{{ $data->id }}"> -->
                                    <input type="hidden" class="form-control"
                                    name="id_pengguna" value="{{ old('auth()->user()->id')}}">          
                <div class="card-header">
                     <h1 class="card-title">{{ ++$key }} . {{ $data->pertanyaan }} <strong> (Dim: {{ $data->dimensi }} | Var: {{ $data->variabel }} )</strong></h1>
                    </div>
                    <div class="card-body">
                    <div class="form-group">
                    <div class="form-check">
                    <input class="form-check-input" type="radio" name="jawaban{{ $data->id }}" id="jawaban{{ $data->id }}" value="A">
                    <label class="form-check-label" for="jawaban{{ $data->id }}">{{ $data->pila }}</label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="radio" name="jawaban{{ $data->id }}" id="jawaban{{ $data->id }}" value="B">
                    <label class="form-check-label" for="jawaban{{ $data->id }}">{{ $data->pilb }}</label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="radio" name="jawaban{{ $data->id }}" id="jawaban{{ $data->id }}" value="C">
                    <label class="form-check-label" for="jawaban{{ $data->id }}">{{ $data->pilc }}</label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="radio" name="jawaban{{ $data->id }}" id="jawaban{{ $data->id }}" value="D">
                    <label class="form-check-label" for="jawaban{{ $data->id }}">{{ $data->pild }}</label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="radio" name="jawaban{{ $data->id }}" id="jawaban{{ $data->id }}" value="E">
                    <label class="form-check-label" for="jawaban{{ $data->id }}">{{ $data->pile }}</label>
                    </div>
                    </div>
                    </div>
                </div>
                
                @endforeach
                <button type="submit" class="btn btn-md btn-primary">Save</button>
                </form>
                
                <div class="pull-right">
                </div>
                <br>
                
</div>

@endsection