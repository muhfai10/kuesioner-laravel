<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: #567189">
    <!-- Brand Logo -->
    <a href="/adminlte/index3.html" class="brand-link">
      <img src="/adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Kuesioner.ai v1</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <!-- <div class="user-panel mt-3 pb-3 mb-3"> -->
      <div class="form-inline mt-3">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
     
      </div>

      <!-- SidebarSearch Form -->
     

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Main Menu
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            @if(auth()->user()->hak_akses == 'Administrator')
              <li class="nav-item">
                <a href="{{ route('dashboard.dashboard') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>My Dashboard</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('dimensi.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Menu Dimensi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('pertanyaan.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Menu Pertanyaan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('pengguna.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Menu Pengguna</p>
                </a>
              </li>
              @else
              <li class="nav-item">
                <a href="{{ route('dashboard.dashboard') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>My Dashboard</p>
                </a>
              </li>
              @if(!$exists)
              <li class="nav-item">
                <a href="{{ route('kuesioner.kuesioner') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Form Kuesioner</p>
                </a>
              </li>
              @endif
              @endif
            </ul>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>