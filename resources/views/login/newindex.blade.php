
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>AdminLTE 3 | Log in (v2)</title>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

<link rel="stylesheet" href="/adminlte/plugins/fontawesome-free/css/all.min.css">

<link rel="stylesheet" href="/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

<link rel="stylesheet" href="/adminlte/dist/css/adminlte.min.css?v=3.2.0">
<script nonce="f4583d68-b3db-461d-b291-a00b24d34d0e">(function(w,d){!function(f,g,h,i){f[h]=f[h]||{};f[h].executed=[];f.zaraz={deferred:[],listeners:[]};f.zaraz.q=[];f.zaraz._f=function(j){return function(){var k=Array.prototype.slice.call(arguments);f.zaraz.q.push({m:j,a:k})}};for(const l of["track","set","debug"])f.zaraz[l]=f.zaraz._f(l);f.zaraz.init=()=>{var m=g.getElementsByTagName(i)[0],n=g.createElement(i),o=g.getElementsByTagName("title")[0];o&&(f[h].t=g.getElementsByTagName("title")[0].text);f[h].x=Math.random();f[h].w=f.screen.width;f[h].h=f.screen.height;f[h].j=f.innerHeight;f[h].e=f.innerWidth;f[h].l=f.location.href;f[h].r=g.referrer;f[h].k=f.screen.colorDepth;f[h].n=g.characterSet;f[h].o=(new Date).getTimezoneOffset();if(f.dataLayer)for(const s of Object.entries(Object.entries(dataLayer).reduce(((t,u)=>({...t[1],...u[1]})))))zaraz.set(s[0],s[1],{scope:"page"});f[h].q=[];for(;f.zaraz.q.length;){const v=f.zaraz.q.shift();f[h].q.push(v)}n.defer=!0;for(const w of[localStorage,sessionStorage])Object.keys(w||{}).filter((y=>y.startsWith("_zaraz_"))).forEach((x=>{try{f[h]["z_"+x.slice(7)]=JSON.parse(w.getItem(x))}catch{f[h]["z_"+x.slice(7)]=w.getItem(x)}}));n.referrerPolicy="origin";n.src="/cdn-cgi/zaraz/s.js?z="+btoa(encodeURIComponent(JSON.stringify(f[h])));m.parentNode.insertBefore(n,m)};["complete","interactive"].includes(g.readyState)?zaraz.init():f.addEventListener("DOMContentLoaded",zaraz.init)}(w,d,"zarazData","script");})(window,document);</script></head>
<body class="hold-transition login-page" style="background-color: #AEE2FF">

<div class="login-box">
@if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-danger" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif
<div class="card card-outline card-primary">
<div class="card-header text-center">
<a href="/adminlte/index2.html" class="h1">Kuesioner<b>.ai</b></a>
</div>
<div class="card-body">
<p class="login-box-msg">Sign in to start your session</p>
<form action="/login" method="post">
@csrf
<div class="input-group mb-3">
<input type="username"d="username" class="form-control @error('username')
      is-invalid @enderror" name="username" value="{{old('username')}}" placeholder="Username" required autofocus>
<div class="input-group-append">
<div class="input-group-text">
<span class="fas fa-envelope"></span>
</div>
</div>
</div>
@error('username')
          <div class="invalid-feedback d-block">
            {{ $message}}
          </div>
      @enderror
<div class="input-group mb-3">
<input type="password"  id="password"  name="password" class="form-control" placeholder="Password" required>
<div class="input-group-append">
<div class="input-group-text">
<span class="fas fa-lock"></span>
</div>
</div>
</div>
<div class="row">

<div class="col-12">
<button type="submit" class="btn btn-primary btn-block">Sign In</button>
<small class="text-center"><a href="/register">Register Now!</a></small>
</div>

</div>
</form>

</div>

</div>

</div>


<script src="/adminlte/plugins/jquery/jquery.min.js"></script>

<script src="/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/adminlte/dist/js/adminlte.min.js?v=3.2.0"></script>

<script>
// setTimeout( myStopFunction, 2500);

// function myStopFunction() {
//  $('#success-alert').hide();
// }

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
    </script>

<script>
// setTimeout( myStopFunction, 2500);

// function myStopFunction() {
//  $('#success-alert').hide();
// }

$("#error-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#error-alert").slideUp(500);
});
    </script>

</body>
</html>
