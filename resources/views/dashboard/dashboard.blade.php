@extends('layouts.maindashboard')

@section('content-header')
<div class="container-fluid">
@if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-error" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif
        <div class="row mb-2">
      
          <div class="col-sm-6">
            <h1>My Dashboard</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="row">
      <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box">
              <span class="info-box-icon"> <img src="/adminlte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"></span>

              <div class="info-box-content">
                <span class="info-box-text">{{auth()->user()->nama_pengguna}}</span>
                <span class="info-box-number">{{auth()->user()->hak_akses}} | Email : {{auth()->user()->email}} </span>
              </div>
              <!-- /.info-box-content -->
            </div>
      </div>

      <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box">
      <span class="info-box-icon bg-success"><i class="fas fa-location-arrow"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{auth()->user()->alamat }} | {{ date('d-m-Y', strtotime(now())) }}</span>
        <span class="info-box-number">Temperature: 26C</span>
      </div>
      </div>
      </div>


      </div>
      </div>
@endsection

@section('content')
<div class="container-fluid">
<h5 class="mb-2">Info Grafik Hasil Kuesioner</h5>
<div class="row">
          <div class="col-md-3">
             <!-- DONUT CHART -->
             <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-globe"></i> Corporate Contribution</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
                <canvas id="myChartCC" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-3">
             <!-- PIE CHART -->
             <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-star"></i> Stakeholder Orientation</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
                <canvas id="myChartSO" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-3">
             <!-- PIE CHART -->
             <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-gear"></i> Operational Excellence</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
                <canvas id="myChartOE" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-3">
             <!-- PIE CHART -->
             <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-line-chart"></i> Future Orientation</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
                <canvas id="myChartFO" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-3">
             <!-- PIE CHART -->
             <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-globe"></i> Corporate Contribution</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
              <div class="col d-flex justify-content-center">
              <input type="text" class="knob1" value="{{ $KPICC }}" data-width="100" data-height="100" data-fgColor="#007bff" data-readonly="true">

              </div>
              <br>
              <div class="col d-flex justify-content-center">
              <div class="knob-label">{{$kesimpulanCC}}</div>
              </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-3">
             <!-- PIE CHART -->
             <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-star"></i> Stakeholder Orientation</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
              <div class="col d-flex justify-content-center">
              <input type="text" class="knob2" value="{{ $KPISO }}" data-width="100" data-height="100" data-fgColor="#28a745" data-readonly="true">

              </div>
              <br>
              <div class="col d-flex justify-content-center">
              <div class="knob-label">{{$kesimpulanSO}}</div>
              </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-3">
             <!-- PIE CHART -->
             <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-gear"></i> Operational Excellence</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
              <div class="col d-flex justify-content-center">
              <input type="text" class="knob3" value="{{ $KPIOE }}" data-width="100" data-height="100" data-fgColor="#dc3545" data-readonly="true">

              </div>
              <br>
              <div class="col d-flex justify-content-center">
              <div class="knob-label">{{$kesimpulanOE}}</div>
              </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-3">
             <!-- PIE CHART -->
             <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-line-chart"></i> Future Orientation</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
              <div class="col d-flex justify-content-center">
              <input type="text" class="knob4" value="{{ $KPIFO }}" data-width="100" data-height="100" data-fgColor="#17a2b8" data-readonly="true">

              </div>
              <br>
              <div class="col d-flex justify-content-center">
              <div class="knob-label">{{$kesimpulanFO}}</div>
              </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
</div>
</div>


</div>  

<script>
    $(function() {
        $(".knob1").knob();
    });

    $(function() {
        $(".knob2").knob();
    });

    $(function() {
        $(".knob3").knob();
    });

    $(function() {
        $(".knob4").knob();
    });
</script>

<!-- Corporate Contribution Data Config -->
<script>
 var ccjawabana = {!! json_encode($ccjawabana->jumlah, JSON_HEX_TAG) !!};
 var ccjawabanb = {!! json_encode($ccjawabanb->jumlah, JSON_HEX_TAG) !!};
 var ccjawabanc = {!! json_encode($ccjawabanc->jumlah, JSON_HEX_TAG) !!};
 var ccjawaband = {!! json_encode($ccjawaband->jumlah, JSON_HEX_TAG) !!};
 var ccjawabane = {!! json_encode($ccjawabane->jumlah, JSON_HEX_TAG) !!};
  
    const data = {
  datasets: [{
    label: 'Jawaban A',
    data: [{x: 1, y: ccjawabana}],
    backgroundColor: 'rgba(255, 26, 104, 0.2)',
    borderColor: 'rgba(255, 26, 104, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban B',
    data:[{x: 2, y: ccjawabanb}],
    backgroundColor: 'rgba(54, 162, 235, 0.2)',
    borderColor: 'rgba(54, 162, 235, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban C',
    data:[{x: 3, y: ccjawabanc}],
    backgroundColor: 'rgba(255, 206, 86, 0.2)',
    borderColor: 'rgba(255, 206, 86, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban D',
    data:[{x: 4, y: ccjawaband}],
    backgroundColor: 'rgba(75, 192, 192, 0.2)',
    borderColor: 'rgba(75, 192, 192, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban E',
    data:[{x: 5, y: ccjawabane}],
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    borderColor: 'rgba(0, 0, 0, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  }
  ]
};

const config = {
  type: 'bar',
  data,
  options: {
    plugins: {
      legend: false,
      tooltip: {
        callbacks: {
          title(tooltipItems){
            if (tooltipItems.length) {
              const item = tooltipItems[0];
              const tick = item.chart.scales.x.ticks[item.datasetIndex]; 
              return tick.label;
            }
          }
        }
      }
    },
    scales: {
      x: {
        labels: ['A', 'B', 'C', 'D', 'E'],
      },
      x1: {
        display: false,
        offset: true
      },
      y: {
            suggestedMin: 0,
            suggestedMax: 200,
            beginAtZero: true,
            min: 0,
            grid: {
               drawOnChartArea: false
            }
          }
      // y1: {
      //       suggestedMin: 0,
      //       suggestedMax: 1000000,
      //       beginAtZero: true,
      //       min: 0,
      //       position: 'right'
      //     }
      }
  }
};

const ctx = document.getElementById("myChartCC");
const myChartCC = new Chart(ctx, config);
</script>

<!-- Stakeholder Orientation Data Config -->
<script>
 var sojawabana = {!! json_encode($sojawabana->jumlah, JSON_HEX_TAG) !!};
 var sojawabanb = {!! json_encode($sojawabanb->jumlah, JSON_HEX_TAG) !!};
 var sojawabanc = {!! json_encode($sojawabanc->jumlah, JSON_HEX_TAG) !!};
 var sojawaband = {!! json_encode($sojawaband->jumlah, JSON_HEX_TAG) !!};
 var sojawabane = {!! json_encode($sojawabane->jumlah, JSON_HEX_TAG) !!};
  
    const data1 = {
  datasets: [{
    label: 'Jawaban A',
    data: [{x: 1, y: sojawabana}],
    backgroundColor: 'rgba(255, 26, 104, 0.2)',
    borderColor: 'rgba(255, 26, 104, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban B',
    data:[{x: 2, y: sojawabanb}],
    backgroundColor: 'rgba(54, 162, 235, 0.2)',
    borderColor: 'rgba(54, 162, 235, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban C',
    data:[{x: 3, y: sojawabanc}],
    backgroundColor: 'rgba(255, 206, 86, 0.2)',
    borderColor: 'rgba(255, 206, 86, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban D',
    data:[{x: 4, y: sojawaband}],
    backgroundColor: 'rgba(75, 192, 192, 0.2)',
    borderColor: 'rgba(75, 192, 192, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban E',
    data:[{x: 5, y: sojawabane}],
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    borderColor: 'rgba(0, 0, 0, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  }
  ]
};

const config1 = {
  type: 'bar',
  data:data1,
  options: {
    plugins: {
      legend: false,
      tooltip: {
        callbacks: {
          title(tooltipItems){
            if (tooltipItems.length) {
              const item = tooltipItems[0];
              const tick = item.chart.scales.x.ticks[item.datasetIndex]; 
              return tick.label;
            }
          }
        }
      }
    },
    scales: {
      x: {
        labels: ['A', 'B', 'C', 'D', 'E'],
      },
      x1: {
        display: false,
        offset: true
      },
      y: {
            suggestedMin: 0,
            suggestedMax: 200,
            beginAtZero: true,
            min: 0,
            grid: {
               drawOnChartArea: false
            }
          }
      // y1: {
      //       suggestedMin: 0,
      //       suggestedMax: 1000000,
      //       beginAtZero: true,
      //       min: 0,
      //       position: 'right'
      //     }
      }
  }
};

const ctx1 = document.getElementById("myChartSO");
const myChartSO = new Chart(ctx1, config1);
</script>

<!-- Future Orientation Data Config -->
<script>
 var fojawabana = {!! json_encode($fojawabana->jumlah, JSON_HEX_TAG) !!};
 var fojawabanb = {!! json_encode($fojawabanb->jumlah, JSON_HEX_TAG) !!};
 var fojawabanc = {!! json_encode($fojawabanc->jumlah, JSON_HEX_TAG) !!};
 var fojawaband = {!! json_encode($fojawaband->jumlah, JSON_HEX_TAG) !!};
 var fojawabane = {!! json_encode($fojawabane->jumlah, JSON_HEX_TAG) !!};
  
    const data3 = {
  datasets: [{
    label: 'Jawaban A',
    data: [{x: 1, y: fojawabana}],
    backgroundColor: 'rgba(255, 26, 104, 0.2)',
    borderColor: 'rgba(255, 26, 104, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban B',
    data:[{x: 2, y: fojawabanb}],
    backgroundColor: 'rgba(54, 162, 235, 0.2)',
    borderColor: 'rgba(54, 162, 235, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban C',
    data:[{x: 3, y: fojawabanc}],
    backgroundColor: 'rgba(255, 206, 86, 0.2)',
    borderColor: 'rgba(255, 206, 86, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban D',
    data:[{x: 4, y: fojawaband}],
    backgroundColor: 'rgba(75, 192, 192, 0.2)',
    borderColor: 'rgba(75, 192, 192, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban E',
    data:[{x: 5, y: fojawabane}],
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    borderColor: 'rgba(0, 0, 0, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  }
  ]
};

const config3 = {
  type: 'bar',
  data:data3,
  options: {
    plugins: {
      legend: false,
      tooltip: {
        callbacks: {
          title(tooltipItems){
            if (tooltipItems.length) {
              const item = tooltipItems[0];
              const tick = item.chart.scales.x.ticks[item.datasetIndex]; 
              return tick.label;
            }
          }
        }
      }
    },
    scales: {
      x: {
        labels: ['A', 'B', 'C', 'D', 'E'],
      },
      x1: {
        display: false,
        offset: true
      },
      y: {
            suggestedMin: 0,
            suggestedMax: 200,
            beginAtZero: true,
            min: 0,
            grid: {
               drawOnChartArea: false
            }
          }
      // y1: {
      //       suggestedMin: 0,
      //       suggestedMax: 1000000,
      //       beginAtZero: true,
      //       min: 0,
      //       position: 'right'
      //     }
      }
  }
};

const ctx3 = document.getElementById("myChartFO");
const myChartFO = new Chart(ctx3, config3);
</script>

<!-- Operational Excellence Data Config -->
<script>
 var oejawabana = {!! json_encode($oejawabana->jumlah, JSON_HEX_TAG) !!};
 var oejawabanb = {!! json_encode($oejawabanb->jumlah, JSON_HEX_TAG) !!};
 var oejawabanc = {!! json_encode($oejawabanc->jumlah, JSON_HEX_TAG) !!};
 var oejawaband = {!! json_encode($oejawaband->jumlah, JSON_HEX_TAG) !!};
 var oejawabane = {!! json_encode($oejawabane->jumlah, JSON_HEX_TAG) !!};
  
    const data2 = {
  datasets: [{
    label: 'Jawaban A',
    data: [{x: 1, y: oejawabana}],
    backgroundColor: 'rgba(255, 26, 104, 0.2)',
    borderColor: 'rgba(255, 26, 104, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban B',
    data:[{x: 2, y: oejawabanb}],
    backgroundColor: 'rgba(54, 162, 235, 0.2)',
    borderColor: 'rgba(54, 162, 235, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban C',
    data:[{x: 3, y: oejawabanc}],
    backgroundColor: 'rgba(255, 206, 86, 0.2)',
    borderColor: 'rgba(255, 206, 86, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban D',
    data:[{x: 4, y: oejawaband}],
    backgroundColor: 'rgba(75, 192, 192, 0.2)',
    borderColor: 'rgba(75, 192, 192, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  },
  {
    label: 'Jawaban E',
    data:[{x: 5, y: oejawabane}],
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    borderColor: 'rgba(0, 0, 0, 1)',
    borderWidth: 1,
    xAxisID: "x1",
    categoryPercentage: 1
  }
  ]
};

const config2 = {
  type: 'bar',
  data:data2,
  options: {
    plugins: {
      legend: false,
      tooltip: {
        callbacks: {
          title(tooltipItems){
            if (tooltipItems.length) {
              const item = tooltipItems[0];
              const tick = item.chart.scales.x.ticks[item.datasetIndex]; 
              return tick.label;
            }
          }
        }
      }
    },
    scales: {
      x: {
        labels: ['A', 'B', 'C', 'D', 'E'],
      },
      x1: {
        display: false,
        offset: true
      },
      y: {
            suggestedMin: 0,
            suggestedMax: 200,
            beginAtZero: true,
            min: 0,
            grid: {
               drawOnChartArea: false
            }
          }
      // y1: {
      //       suggestedMin: 0,
      //       suggestedMax: 1000000,
      //       beginAtZero: true,
      //       min: 0,
      //       position: 'right'
      //     }
      }
  }
};

const ctx2 = document.getElementById("myChartOE");
const myChartOE = new Chart(ctx2, config2);
</script>

<script>
// setTimeout( myStopFunction, 2500);

// function myStopFunction() {
//  $('#success-alert').hide();
// }

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
    </script>
@endsection
