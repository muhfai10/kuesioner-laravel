<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\DimensiController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\KuesionerController;
use App\Http\Controllers\PertanyaanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('pengguna', PenggunaController::class);
Route::resource('dimensi', DimensiController::class);
Route::resource('pertanyaan', PertanyaanController::class);

Route::get('/login', [LoginController::class, 'index'] );
Route::post('/login', [LoginController::class, 'authenticate'] );
Route::post('/logout', [LoginController::class, 'logout'] );

Route::get('/kuesioner', [KuesionerController::class, 'getAllKuesioner'] )->name('kuesioner.kuesioner');
Route::post('/store', [KuesionerController::class, 'store'] )->name('jawaban.store');

Route::get('/dashboard', [LoginController::class, 'afterLogin'] )->name('dashboard.dashboard');
