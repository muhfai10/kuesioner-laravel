-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Feb 2023 pada 19.08
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tibsc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbdimensi`
--

CREATE TABLE `tbdimensi` (
  `id` int(11) NOT NULL,
  `dimensi` varchar(50) DEFAULT NULL,
  `bobot` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbdimensi`
--

INSERT INTO `tbdimensi` (`id`, `dimensi`, `bobot`, `created_at`, `updated_at`) VALUES
(1, 'CORPORATE CONTRIBUTION', 3, '2023-02-04 17:02:01', '2023-02-04 10:23:24'),
(2, 'STAKEHOLDER (USER) ORIENTATION', 4, '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(3, 'OPERATIONAL EXCELLENCE (KEUNGGULAN OPERASIONAL) ', 4, '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(4, 'FUTURE ORIENTATION (Orientasi Masa Depan)', 4, '2023-02-04 17:02:01', '2023-02-04 17:02:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjawaban`
--

CREATE TABLE `tbjawaban` (
  `id` int(11) NOT NULL,
  `id_kuesioner` int(11) DEFAULT NULL,
  `jawaban` varchar(5) DEFAULT NULL,
  `id_pengguna` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbjawaban`
--

INSERT INTO `tbjawaban` (`id`, `id_kuesioner`, `jawaban`, `id_pengguna`, `created_at`, `updated_at`) VALUES
(267, 1, 'A', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(268, 2, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(269, 3, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(270, 4, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(271, 5, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(272, 6, 'E', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(273, 7, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(274, 8, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(275, 9, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(276, 10, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(277, 11, 'A', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(278, 12, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(279, 13, 'E', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(280, 14, 'A', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(281, 15, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(282, 16, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(283, 17, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(284, 18, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(285, 19, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(286, 20, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(287, 21, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(288, 22, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(289, 23, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(290, 24, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(291, 25, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(292, 26, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(293, 28, 'A', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(294, 29, 'A', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(295, 30, 'E', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(296, 31, 'E', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(297, 32, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(298, 33, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(299, 34, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(300, 35, 'A', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(301, 36, 'E', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(302, 37, 'E', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(303, 38, 'A', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(304, 39, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(305, 40, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(306, 41, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(307, 42, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(308, 43, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(309, 44, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(310, 45, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(311, 46, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(312, 47, 'E', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(313, 48, 'A', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(314, 49, 'E', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(315, 50, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(316, 51, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(317, 52, 'A', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(318, 53, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(319, 54, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(320, 55, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(321, 56, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(322, 57, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(323, 58, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(324, 59, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(325, 60, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(326, 61, 'C', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(327, 62, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(328, 63, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(329, 64, 'D', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(330, 65, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(331, 66, 'B', 3, '2023-02-05 07:21:08', '2023-02-05 07:21:08'),
(332, 1, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(333, 2, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(334, 3, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(335, 4, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(336, 5, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(337, 6, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(338, 7, 'C', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(339, 8, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(340, 9, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(341, 10, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(342, 11, 'D', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(343, 12, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(344, 13, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(345, 14, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(346, 15, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(347, 16, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(348, 17, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(349, 18, 'C', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(350, 19, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(351, 20, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(352, 21, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(353, 22, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(354, 23, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(355, 24, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(356, 25, 'C', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(357, 26, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(358, 27, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(359, 28, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(360, 29, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(361, 30, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(362, 31, 'D', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(363, 32, 'C', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(364, 33, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(365, 34, 'D', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(366, 35, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(367, 36, 'D', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(368, 37, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(369, 38, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(370, 39, 'D', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(371, 40, 'E', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(372, 41, 'D', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(373, 42, 'D', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(374, 43, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(375, 44, 'C', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(376, 45, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(377, 46, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(378, 47, 'C', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(379, 48, 'D', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(380, 49, 'C', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(381, 50, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(382, 51, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(383, 52, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(384, 53, 'D', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(385, 54, 'C', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(386, 55, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(387, 56, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(388, 57, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(389, 58, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(390, 59, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(391, 60, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(392, 61, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(393, 62, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(394, 63, 'B', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(395, 64, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(396, 65, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(397, 66, 'A', 4, '2023-02-05 13:21:46', '2023-02-05 13:21:46'),
(398, 1, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(399, 2, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(400, 3, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(401, 4, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(402, 5, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(403, 6, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(404, 7, 'C', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(405, 8, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(406, 9, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(407, 10, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(408, 11, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(409, 12, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(410, 13, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(411, 15, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(412, 16, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(413, 17, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(414, 18, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(415, 19, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(416, 20, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(417, 21, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(418, 22, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(419, 23, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(420, 24, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(421, 25, 'C', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(422, 26, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(423, 27, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(424, 28, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(425, 29, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(426, 30, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(427, 31, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(428, 32, 'C', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(429, 33, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(430, 34, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(431, 35, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(432, 36, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(433, 37, 'C', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(434, 38, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(435, 39, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(436, 40, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(437, 41, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(438, 42, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(439, 43, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(440, 44, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(441, 45, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(442, 46, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(443, 47, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(444, 48, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(445, 49, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(446, 50, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(447, 51, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(448, 52, 'C', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(449, 53, 'E', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(450, 54, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(451, 55, 'A', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(452, 56, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(453, 57, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(454, 58, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(455, 59, 'C', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(456, 60, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(457, 61, 'D', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(458, 62, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(459, 63, 'C', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(460, 64, 'C', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(461, 65, 'B', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55'),
(462, 66, 'C', 5, '2023-02-05 16:44:55', '2023-02-05 16:44:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbkuesioner`
--

CREATE TABLE `tbkuesioner` (
  `id` int(11) NOT NULL,
  `pertanyaan` varchar(255) DEFAULT NULL,
  `id_dimensi` int(11) DEFAULT NULL,
  `variabel` varchar(50) DEFAULT NULL,
  `pila` varchar(255) DEFAULT NULL,
  `pilb` varchar(255) DEFAULT NULL,
  `pilc` varchar(255) DEFAULT NULL,
  `pild` varchar(255) DEFAULT NULL,
  `pile` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbkuesioner`
--

INSERT INTO `tbkuesioner` (`id`, `pertanyaan`, `id_dimensi`, `variabel`, `pila`, `pilb`, `pilc`, `pild`, `pile`, `created_at`, `updated_at`) VALUES
(1, 'Berapa persen kira-kira dana pengembangan TIK yang dialokasikan oleh kampus dari pengeluaran yang ada ?', 1, 'komitmen pimpinan', 'Dibawah 5%', 'Antara 5%-10%', 'Antara 11%-20%', 'Antara 21%-30%', 'Diatas 30%', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(2, 'Apakah perguruan tinggi memiliki program sosialisasi dan edukasi mengenai pentingnya pemanfaatan TIK kampus?', 1, 'komitmen pimpinan', 'Ada, namun bersifat sporadis dan tidak terencana', 'Ada, dengan ruang lingkup dan target sasaran audience sesuai dengan anggaran', 'Ada, terencana dengan baik dilaksanakan secara konsisten, kontinyu dan berkesinambungan', 'Ada, terencana dengan baik dan dilaksanakan secara konsisten, kontinyu dan berkesinambungan dan mendapatkan banyak bantuan serta didukung dari stakeholder lain diluar perangkat organisasi perguruan', 'Ada, terencana dengan baik dan dilaksanakan secara konsisten, kontinyu dan berkesinambungan dan mendapatkan banyak bantuan serta didukung dari stakeholder lain diluar perangkat organisasi perguruan ti', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(3, 'Apakah perguruan tinggi menggunakan media tertentu dalam melakukan sosialisasi dan edukasi mengenai pentingnya pemanfaatan teknologi informasi dan komunikasi kampus ?', 1, 'komitmen pimpinan', 'Tidak', 'Ya, di beberapa media namun dalam ukuran kecil', 'Ya, di sejumlah media dan cukup efektif', 'Ya, di sejumlah media dan sangat signifikan dampaknya', 'Ya, cukup banyak jalur media yang memberikan kontribusi positif dan signifikan terhadap perkembangan TIK perguruan tinggi.', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(4, 'Dari manasajakah sumber dana terbesar yang diperuntukkan untuk melakukan investasi dalam membangun TIK Perguruan tinggi ?', 1, 'alokasi sumber daya', 'Tidak dari mana-mana, karena memang belum memiliki TIK', 'Diambil melalui dana yang berasal dari uang bayaran mahasiswa, baik yang berupa sumbangan maupun uang kuliah', 'Dialokasikan khusus oleh pemilik perguruan tinggi dalam bentuk belanja modal', 'Sebagian dari dana bantuan pemerintah Indonesia dan sisa hasil usaha penyelenggaraan belajar mengajar', 'Investasi pihak ketiga (eksternal) yang memiliki komitme penuh jangka panjang dalam membangun perguruan tinggi yang ada.', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(5, 'Berapa kira-kira nilai investasi TIK dalam tiga tahun terakhir ?', 1, 'alokasi sumber daya', 'Kurang dari Rp. 100 juta', 'Rp. 100 â€“ 500 juta', 'Rp. 500 juta â€“ 1 Milyar', 'Rp. 1-3 Milyar', '> Rp. 3 Milyar', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(6, 'Berapa jumlah karyawan yang berlatar belakang pendidikan perguruan tinggi bidang komputer atau informatika ?', 1, 'alokasi sumber daya', 'Kurang dari 1% dari keseluruhan pegawai di Perguruan tinggi', '2%-5% dari keseluruhan pegawai di Perguruan tinggi', '6%-15% dari keseluruhan pegawai di Perguruan tinggi', '16%-35% dari keseluruhan pegawai di Perguruan tinggi', '>35% dari keseluruhan pegawai di Perguruan tinggi', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(7, 'Apakah perguruan tinggi memiliki unit unit atau divisi khusus yang bertanggung jawab terhadap perencanaan, pembangunan, penerapan, pengendalian  dan pengembangan TIK di lingkungan kampus ?', 1, 'unit pengelola teknologi', 'Tidak ada', 'Ada, namun tidak beroperasi secara optimal karena masih mengerjakan program atau proyek', 'Ada, dan merupakan suatu unit organisasi tersendiri dengan tanggung jawab yang jelas', 'Ada, merupakan suatu unit organisasi tersendiri, yang berjalan berdasarkan tanggung jawab yang jelas sesuai dengan SOP yang telah disusun dan dikembangkan', 'Ada, merupakan sebuah unit organisasi independen, dimana didukung oleh berbagai pihak ketiga yang sesuai dengan bidang keahliannya, dibawah tata kelola manajemen yang rapi dan transparan.', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(8, 'Jika perguruan tinggi memiliki unit khusus yang dibentuk oleh pihak kampus untuk mengelola TIK yang dimiliki, dibawah komando siapa unit khusus ini berada ?', 1, 'unit pengelola teknologi', 'Tidak dibawah siapa-siapa, karena tidak terdapat unit yang dimaksud', 'Berada dibawah komando salah satu unit kegiatan yang ada di perguruan tinggi', 'Berada dibawah komando salah satu divisi kegiatan yang ada di perguruan tinggi', 'Berada dibawah komando salah satu wakil pimpinan perguruan tinggi institusi', 'Langsung dibawah komando pimpinan perguruan tinggi institusi', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(9, 'Apakah perguruan tinggi memiliki unit atau divisi yang bertanggung jawab terhadap pelatihan TIK dalam lingkungan kampus ?', 1, 'unit pengelola teknologi', 'Tidak ada', 'Ada, namun tidak berorientasi secara optimal karena masih mengerjakan hal-hal yang bersifat ad-hoc, atau berbasis program atau proyek', 'Ada, dan merupakan suatu unit organisasi tersendiri dengan tanggung jawab yang jelas', 'Ada, merupakan suatu unit organisasi tersendiri, yang berjalan berdasarkan tanggung jawab yang jelas sesuai dengan SOP yang telah disusun dan dikembangkan.', 'Ada, merupakan sebuah unit organisasi independen, dimana didukung oleh berbagai pihak ketiga yang sesuai dengan bidang keahliannya, di bawah tata kelola manajemen yang rapi dan transparan', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(10, 'Apakah perguruan tinggi memiliki kebijakan dan peraturan yang mengharuskan setiap civitas akademika memanfaatkan secara intensif TIK dalam lingkungan kampus sebagai penunjang kegiatan belajar mengajar', 1, 'kebijakan dan sistem insentif', 'Tidak memiliki', 'Tidak memiliki secara tertulis, namun sering disampaikan dan diwacanakan oleh segenap pimpinan', 'Terdapat kebijakan tertulis melalui surat keputusan pimpinan perguruan tinggi mengenai keharusan menggunakan TIK dalam lingkungan kampus.', 'Terdapat sejumlah kebijakan terkait dengan pemanfaatan TIK sesuai tata aturan yang ada, dan dijadikan sebagai salah satu pengukur kinerja unit maupun individu terkait', 'Terdapat berbagai kebijakan dan peraturan keharusan pemanfaatan TIK dalam lingkungan kampus dipergunakan sebagai komponen pengukur kinerja dan terdapat model insentif yang diterapkan manajemen kampus ', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(11, 'Apakah ada sistem insentif yang dikembangkan oleh perguruan tinggi untuk mendorong agar segenap civitas akademika di kampus memanfaatkan teknologi informasi dan komunikasi seoptimal mungkin ?', 1, 'kebijakan dan sistem insentif', 'Tidak ada', 'Ada, namun tidak jelas dan konsisten penerapannya', 'Ada, dimana implementasinya diatur dalam peraturan perguruan tinggi', 'Ada, dimana implementasinya telah terintegrasi dengan sistem pengelolaan sumber daya manusia yang dimiliki oleh perguruan tinggi', 'Ada, dimana model dan implementasinya selain telah terintegrasi secara baik dengan sistem sumber daya manusia, tingkat efekivitasnya pun diukur dan dinilai dari hari ke hari.', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(12, 'Bagaimana karyawan dan staf perguruan tinggi belajar mengenai cara memanfaatkan teknologi informasi dan komunikasi (missal : menggunakan computer dan internet) ?', 1, 'kebijakan dan sistem insentif', 'Kebanyakan belajar sendiri (mandiri) atau otodidak', 'Kebanyakan belajar dari orang lain dan sumber referensi (buku)', 'Kebanyakan belajar dari orang lain, sumber referensi dan mengikuti pendidikan non formal (pelatihan dan kursus)', 'Kebanyakan belajar dari orang lain, sumber referensi dan mengikuti pendidikan formal dan vokasi', 'Kebanyakan belajar dari orang lain, sumber referensi, mengikuti pendidikan formal (akademik maupun vokasi) maupun informal.', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(13, 'Bagaimana karyawan dan staf perguruan tinggi belajar mengenai cara memanfaatkan teknologi informasi dan komunikasi (misal : menggunakan komputer dan internet) ?', 1, 'kebijakan dan sistem insentif', 'Kebanyakan belajar sendiri (mandiri) atau otodidak', 'Kebanyakan belajar dari orang lain dan sumber referensi (buku)', 'Kebanyakan belajar dari orang lain, sumber referensi dan mengikuti pendidikan non formal (pelatihan dan kursus)', 'Kebanyakan belajar dari orang lain, sumber referensi dan mengikuti pendidikan formal dan vokasi', 'Kebanyakan belajar dari orang lain, sumber referensi, mengikuti pendidikan formal (akademik maupun vokasi) maupun informal', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(14, 'Apakah perguruan tinggi telah memiliki dan menyusun Rencana Strategis Pengembangan TIK (IT Master Plan) untuk kebutuhan kampus ?', 1, 'Renstra dan peta jalan', 'Tidak memiliki', 'Memiliki, namun sudah lama tidak dimutakhirkan (update)', 'Memiliki dan setiap tahun diperbaharui', 'Memiliki dan secara konsisten seluruh program pengembangan TIK kampus didasari pada dokumen ini.', 'Memiliki dan secara konsisten seluruh program pengembangan TIK kampus didasari pada dokumen ini dan dilakukan audit terhadap efektivitas kepatuhannya.', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(15, 'Apakah Rencana Strategis Pengembangan TIK (IT Master Plan) yang dimiliki dipergunakan sebagai panduan / acuan dalam menyelenggarakan berbagai proyek kegiatan teknologi informasi dan komunikasi perguru', 1, 'Renstra dan peta jalan', 'Tidak karena memang belum memiliki Rencana Strategis Pengembangan IT (IT Master Plan)', 'Ya, namun baru sebagian kecil saja proyek atau kegiatan yang mengacu pada dokumen ini.', 'Ya, rata-rata proyek atau kegiatan mengacu pada dokumen ini', 'Ya, sebagian besar proyek atau kegiatan mengacu pada dokumen ini.', 'Ya, semua proyek mengacu pada dokumen ini, jika ada proyek yang tidak termasuk maka menjadi bahan masukan untuk merevisi dokumen yang ada.', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(16, 'Bagaimana cara perguruan tinggi menyusun kebutuhan teknologi informasi dan komunikasi yang diinginkan ? ', 1, 'Perencanaan dan pengorganisasian', 'Berkaca pada perguruan tinggi lain, dan mencoba mengikutinya.', 'Menyusun kebutuhan TIK berdasarkan keinginan orang yang menangani TIK', 'Menyusun kebutuhan TIK berdasarkan jumlah unit-unit yang membutuhkan TIK', 'Menyusun kebutuhan TIK berdasarkan analisis kebutuhan masing-masing unit', 'Menyusun kebutuhan TIK berdasarkan analisis kebutuhan masing-masing unit dan senantiasa diperbaharui dan dimutakhirkan sesuai perkembangan organisasi.', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(17, 'Apakah perguruan tinggi memiliki dokumen arsitektur yang dipergunakan sebagai panduan atau acuan teknis pembangunan teknologi informasi dan komunikasi di perguruan tinggi ?', 1, 'Perencanaan dan pengorganisasian', 'Tidak memiliki', 'Ya, namun sebagian kecil inisiatif pembangunan teknologi informasi dan komunikasi yang mengadu pada rancangan arsitektur ini', 'Ya, rata-rata inisiatif pembangunan teknologi informasi dan komunikasi mengacu pada rancangan arsitektur ini.', 'Ya, sebagian besar inisiatif pembangunan teknologi informasi dan komunikasi mengacu pada rancangan arsitektur ini', 'Ya, semua inisiatif pembangunan teknologi informasi dan komunikasi mengacu pada rancangan arsitektur ini.', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(18, 'Apakah perguruan tinggi anda memiliki standar terkait teknologi informasi dan komunikasi di perguruan tinggi yang akan diadopsi (missal : standar dalam tipe aplikasi, fitur / spesifikasi piranti keras', 1, 'Perencanaan dan pengorganisasian', 'Tidak ada sama sekali', 'Ada, kadang-kadang dipergunakan, kadang-kadang tidak', 'Ada, dipergunakan secara konsisten sebagai basis pengelolaan dan dikembangkan secara kontinyu dan berkesinambungan', 'Ada, dipergunakan secraa konsisten sebagai basis pengelolaan dan dikembangkan secara kontinyu dan berkesinambungan', 'Ada, dipergunakan secara konsisten sebagai basis pengelolaan, dikembangkan secara kontinyu dan berkesinambungan, dan dievaluasi / diaudit kepatuhan adopsinya', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(19, 'Apakah memiliki mekanisme penghitungan cost benefit dari setiap program, atau inisiatif investasi pengembangan teknologi informasi dan komunikasi di perguruan tinggi ?', 1, 'Perencanaan dan pengorganisasian', 'Tidak ada, namun sudah dipikirkan untuk menyusunnya', 'Ada, namun belum secara konsisten diadopsi sepenuhnya', 'Ada, dan dijadikan sebagai acuan baku pelaksanaan manajemen proyek teknologi informasi', 'Ada, dipergunakan sebagai acuan baku pelaksanaan manajemen proyek teknologi informasi dan dijadikan basis evaluasi kinerja perangkat organisasi perguruan tinggi', 'Ada, dipergunakan sebagai acuan baku pelaksanaan manajemen proyek teknologi informasi dan dijadikan basis evaluasi kinerja perguruan tinggi dan senantiasa dikembangkan / direvisi mekanisme baku yang d', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(20, 'Apakah segenap pimpinan perguruan tinggi secara intensif menggunakan beragam aplikasi teknologi informasi dan komunikasi dalam aktivitas sehari-harinya ?', 1, 'Pengadaan dan penerapan ', 'Tidak sama sekali', 'Ya, menggunakan namun tidak optimal dan tidak intensif', 'Ya, menggunakan secara efektif dan cukup intensif', 'Ya. menggunakan secara sangat intensif, efektif dan optimal', 'Ya, menggunakan secara sangat intensif, efektif, optimal dan telah berhasil menularkan kebiasaan ini kepada segenap pimpinan manajerial di bawahnya', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(21, 'Apakah suasana atau budaya memanfaatkan teknologi informasi dan komunikasi terasa di dalam lingkungan kampus ?', 1, 'Pengadaan dan penerapan', 'Tidak sama sekali', 'Cukup terasa namun dalam keadaan atau waktu-waktu tertentu saja', 'Cukup terasa dalam situasi sehari-hari', 'Ya, sangat terasa karena hampir seluruh lokasi strategis Nampak segenap civitas akademika memanfaatkan teknologi', 'Ya, sangat terasa karena secara massif terlihat dari aktivitas segenap civitas akademika yang berinteraksi dengan teknologi di setiap sudut-sudut kampus', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(22, 'Apakah perguruan tinggi memiliki dokumen yang jelas dan detail (misal : standar operasional prosedur) dari proses pengelolaan teknologi informasi dan komunikasi di perguruan tinggi ?', 1, 'Pengelolaan dan pengembangan', 'Tidak ada, semua berjalan seperti kebiasaan saja', 'Tidak ada, semua berjalan seperti kebiasaan saja', 'Ada, dipergunakan sebagai panduan kerja namun tidak pernah dikaji maupun dimutakhirkan', 'Ada, dipergunakan sebagai panduan kerja senantiasa dimutakhirkan sesuai dengan perbaikan dan dipakai sebagai basis evaluasi kinerja', 'Ada, dipergunakan sebagai panduan kerja senantiasa dimutakhirkan sesuai dengan perbaikan dan dipakai sebagai basis evaluasi kinerja dan penentuan remunerasi berdasarkan beban kerja dan tanggung jawab', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(23, 'Apakah memiliki standar baku dalam manajemen pengelolaan proyek (project management) teknologi informasi dan komunikasi di perguruan tinggi ?', 1, 'Pengelolaan dan pengembangan', 'Tidak ada, namun sudah dipikirkan untuk menyusunnya', 'Ada, namun belum secara konsisten diadopsi sepenuhnya', 'Ada, dan dijadikan acuan baku pelaksanaan manajemen proyek teknologi informasi dan dijadikan basis evaluasi kinerja perangkat organisasi perguruan tinggi', 'Ada, dipergunakan sebagai acuan baku pelaksanaan manajemen proyek teknologi informasi dan dijadikan basis kinerja perangkan organisasi perguruan tinggi', 'Ada, dipergunakan sebagai acuan baku pelaksanaan manajemen proyek teknologi informasi dan dijadikan basis kinerja perangkan organisasi perguruan tinggi dan senantiasa dikembangkan / direvisi mekanisme', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(24, 'Apakah perguruan tinggi anda memiliki prosedur penanganan keadaan darurat (contingency plan) bila terjadi gangguan pada sistem teknologi informasi dan komunikasi ?', 1, 'Pengelolaan dan pengembangan', 'Tidak ada, semua berjalan secara naluri dan kebiasaan saja', 'Ada, dipergunakan sebagai panduan kerja namun tidak pernah dikaji dan dimutakhirkan', 'Ada, dipergunakan sebagai panduan kerja, senantiasa dimutakhirkan sesuai dengan perbaikan', 'Ada, dipergunakan sebagai panduan kerja, senantiasa dimutakhirkan sesuai dengan perbaikan, dan dipakai sebagai basis evaluasi kinerja', 'Ada, dipergunakan sebagai panduan kerja, senantiasa dimutakhirkan sesuai dengan perbaikan, dan dipakai sebagai basis evaluasi kinerja serta penentuan remunerasi berdasarkan beban kerja dan tanggung ja', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(25, 'Apakah tersedia pelayanan “help desk” untuk membantu para pengguna teknologi informasi dan komunikasi yang memiliki masalah ?', 1, 'Pengelolaan dan pengembangan', 'Tidak tersedia', 'Tersedia, namun kinerjanya tidak seperti yang diharapkan', 'Tersedia dan beroperasi dengan baik pada jam kerja', 'Tersedia dan beroperasi dengan baik selama 24 jam sehari, kecuali hari libur atau Sabtu / Minggu', 'Tersedia dan beroperasi dengan baik selama 24 jam sehari, dan tujuh hari seminggu', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(26, 'Apakah dilakukan proses audit terhadap efektivitas penerapan teknologi informasi dan komunikasi di lingkungan kampus ?', 1, 'Pemantauan dan penilaian', 'Tidak sama sekali', 'Pada dasarnya dilakukan evaluasi secara umum, namun tidak berdasarkan prosedur baku audit', 'Ya, dilakukan audit oleh pihak internal yang berkepentingan dan memiliki kapabilitas', 'Ya, dilakukan audit oleh pihak eksternal yang berkepentingan dan memiliki kapabilitas', 'Ya, dilakukan audit oleh pihak internal dan eksternal secara independen dan bertanggung jawab', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(27, 'Apakah perguruan tinggi memiliki indikator kinerja utama (Key Performance Indicator) dalam implementasi teknologi informasi dan komunikasi kampus ?', 1, 'Pemantauan dan penilaian', 'Tidak ada, semuanya berjalan secara naluri dan kebiasaan', 'Ada, dipergunakan sebagai panduan kerja, namun tidak pernah dikaji maupun dimutakhirkan', 'Ada, dipergunakan sebagai panduan kerja, senantiasa dimutakhirkan sesuai dengan perbaikan', 'Ada, dipergunakan sebagai panduan kerja, senantiasa dimutakhirkan sesuai dengan perbaikan dan dipakai sebagai basis evaluasi kinerja', 'Ada, dipergunakan sebagai panduan kerja, senantiasa dimutakhirkan sesuai dengan perbaikan dan dipakai sebagai basis evaluasi kinerja dan penentuan remunerasi berdasarkan beban kerja dan tanggung jawab', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(28, 'Berapa persen kira-kira materi perkuliahan yang telah berbentuk file elektronik atau digital ?', 2, 'Dosen dan peneliti', 'Dibawah 10%', 'Kira-kira antara 11%-25%', 'Kira-kira antara 26%-50%', 'Kira-kira antara 51%-75%', 'Kira-kira diatas 76%', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(29, 'Berapa persen kira-kira materi perkuliahan yang dibawakan oleh dosen telah memanfaatkan dan melibatkan teknologi informasi dan komunikasi untuk mempermudah pemahaman mahasiswa sebagai peserta ajar ?', 2, 'Dosen dan peneliti', 'Dibawah 10%', 'Kira-kira antara 11%-25%', 'Kira-kira antara 26%-50%', 'Kira-kira antara 51%-75%', 'Kira-kira diatas 76%', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(30, 'Berapa persen kira-kira jumlah mata kuliah yang telah dilakukan dengan menggunakan metode e-learning ?', 2, 'Dosen dan peneliti', 'Tidak ada sama sekali', 'Kira-kira dibawah 10%', 'Kira-kira antara 11%-25%', 'Kira-kira antara 26%-50%', 'Kira-kira diatas 50%', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(31, 'Berapa persen kira-kira jumlah mata kuliah dimana sang dosen memberikan pekerjaan rumah yang memaksa mahasiswanya untuk menggunakan teknologi informasi dan komunikasi secara intensif ?', 2, 'Dosen dan peneliti', 'Tidak ada sama sekali', 'Kira-kira dibawah 10%', 'Kira-kira dibawah 10%', 'Kira-kira antara 26%-50%', 'Kira-kira diatas 50%', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(32, 'Bagaimana tingkat literasi dosen dalam memanfaatkan berbagai aplikasi teknologi informasi dan komunikasi ?', 2, 'Dosen dan peneliti', 'Rata-rata sangat rendah, karena kebanyakan masih gagap teknologi', 'Rata-rata cukup mahir menggunakan komputer, hanya saja kebanyakan diperuntukkan dalam mendukung kegiatan administratif (mengetik, mengirim email, membuat tabulasi, dan lain-lain)', 'Rata-rata cukup mahir dan menguasai berbagai aplikasi standar yang dipergunakan oleh seorang pengajar untuk mendukung aktivitasnya sehari-hari', 'Kebanyakan dosen telah sangat mahir dalam menggunakan beragam aplikasi standar maupun aplikasi termutakhir yang dikenal masyarakat umum', 'Mayoritas dosen telah sangat mahir dalam menggunakan beragam aplikasi standar maupun aplikasi termutakhir yang dikenal masyarakat umum, bahkan cenderung lebih tinggi tingkat  literasinya dibandingkan ', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(33, 'Apakah terjadi komunikasi yang intens antara dosen dan mahasiswa melalui kanal dan aplikasi teknologi informasi dan komunikasi seperti email, mailing list, newsgroup, social networking dan lain-lain ?', 2, 'Dosen dan peneliti', 'Tidak sama sekali', 'Terjadi komunikasi virtual, namun tidak terlampau intensif', 'Terjadi komunikasi virtual yang cukup intensif', 'Hampir setiap hari rata-rata mahasiswa dan dosen melakukan interaksi secara virtual', 'Setiap hari mayoritas mahasiswa dan dosen melakukan interaksi secara virtual', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(34, 'Berapa jumlah kelas yang pada saat diselenggarakan sang dosen membentuk sebuah mailing list khusus untuk para mahasiswa yang mengambil mata kuliah yang bersangkutan ?', 2, 'Dosen dan peneliti', 'Dibawah 5% dari total kelas yang diikuti mahasiswa selama masa kuliah', 'Kira-kira antara 6%-15% dari total kelas yang diikuti mahasiswa selama masa kuliah', 'Kira-kira antara 16%-30% dari total kelas yang diikuti mahasiswa selama masa kuliah', 'Kira-kira antara 31%-60% dari total kelas yang diikuti mahasiswa selama masa kuliah', 'Diatas 6%-15% dari total kelas yang diikuti mahasiswa selama masa kuliah', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(35, 'Berapa persen dari dosen yang memiliki alamat email yang aktif dipergunakan?', 2, 'Dosen dan peneliti', 'Dibawah 10%', 'Kira-kira antara 11%-20%', 'Kira-kira antara 21%-50%', 'Kira-kira antara 51%-75%', 'Diatas antara 75%', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(36, 'Berapa persen dari dosen yang memiliki website / homepage atau blog pribadi?', 2, 'Dosen dan peneliti', 'Dibawah 10%', 'Kira-kira antara 11%-20%', 'Kira-kira antara 21%-50%', 'Kira-kira antara 51%-75%', 'Diatas antara 75%', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(37, 'Rata-rata berapa jam dalam sehari seorang dosen berinteraksi dengan internet?', 2, 'Dosen dan peneliti', 'Hanya dibawah 1 jam per hari', 'Kurang lebih antara 1-2 jam per hari', 'Kurang lebih antara 2-4 jam per hari', 'Kurang lebih antara 4-6 jam per hari', 'Hampir diatas 6 jam per hari', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(38, 'Berapa persen dari mahasiswa yang memiliki alamat email yang aktif dipergunakan ?', 2, 'Mahasiswa, unsur pemilik dan pimpinan', 'Dibawah 10%', 'Kira-kira antara 11%-20%', 'Kira-kira antara 21%-50%', 'Kira-kira antara 51%-75%', 'Diatas antara 75%', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(39, 'Rata-rata berapa jam dalam sehari seorang mahasiswa berinteraksi dengan internet ?', 2, 'Mahasiswa, unsur pemilik dan pimpinan', 'Hanya dibawah 1 jam per hari', 'Kurang lebih antara 1-2 jam per hari', 'Kurang lebih antara 2-4 jam per hari', 'Kurang lebih antara 4-6 jam per hari', 'Hampir diatas 6 jam per hari', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(40, 'Berapa persen dari karyawan yang memiliki alamat email yang aktif dipergunakan ?', 2, 'Manajemen, staf dan karyawan', 'Dibawah 10%', 'Kira-kira antara 11%-20%', 'Kira-kira antara 21%-50%', 'Kira-kira antara 51%-75%', 'Diatas antara 75%', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(41, 'Bagaimana tingkat literasi karyawan dalam memanfaatkan berbagai aplikasi teknologi informasi dan komunikasi untuk membantu kegiatan administratif dan operasional kampus ?', 2, 'Manajemen, staf dan karyawan', 'Rata-rata sangat rendah, karena kebanyakan masih “gagap teknologi”', 'Rata-rata cukup mahir menggunakan satu atau dua aplikasi komputer', 'Rata-rata cukup mahir dan menguasai sejumlah aplikasi komputer untuk kegiatan operasional organisasi', 'Kebanyakan karyawan telah mahir dan menguasai dan menggunakan berbagai aplikasi komputer perkantoran maupun penunjang lainnya', 'Mayoritas karyawan sangat mahir dalam memanfaatkan dan menggunakan beraneka ragam aplikasi teknologi dan komunikasi', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(42, 'Apakah anda merasa keberadaan teknologi infomasi dan komunikasi akan memberikan manfaat dan kontribusi signifikan bagi perkembangan perguruan tinggi anda ?', 3, 'Peningkatan kualitas', 'Ya, keberadaannya akan memberikan kontribusi positif namun tidak signifikan', 'Ya, keberadaannya akan memberikan kontribusi positif, namun signifikan tidaknya bergantung dari sudut pandang yang dipergunakan', 'Ya keberadaan akan memberikan kontribusi positif dan signifikan bagi perguruan tinggi ', 'Ya keberadaan akan memberikan kontribusi positif dan signifikan bagi perguruan tinggi bahkan dapat meningkatkan daya saing jika direncanakan dan diterapkan dengan benar', 'Ya keberadaan akan memberikan kontribusi positif dan signifikan bagi perguruan tinggi dan sanggup mentransformasikan situasi dan kondisi perguruan tinggi ke arah modernisasi kampus', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(43, 'Apakah ada kontribusi nyata terkait dengan teknologi informasi dan komunikasi dalam hal peningkata kualitas pengambilan keputusan para pembuat kebijakan di perguruan tinggi anda dalam 5 tahun terakhir', 3, 'Peningkatan kualitas', 'Tidak ada kontribusi yang signifikan', 'Ada sejumlah kontribusi positif, namun tidak signifikan', 'Ada kontribusi positif dan cukup signifikan', 'Jelas terlihat adanya kontribusi yang positif dan signifikan serta dapat diukur besaran peningkatannya', 'Jelas terlihat adanya kontribusi yang positif dan signifikan dapat diukur besaran peningkatannya dan memberikan manfaat langsung bagi para mahasiswa', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(44, 'Apakah ada kontribusi nyata terkait dengan teknologi informasi dan komunikasi dalam hal peningkatan kualitas pelayanan di kalangan para mahasiswa di perguruan tinggi anda dalam 5 tahun terakhir ?', 3, 'Peningkatan kualitas', 'Tidak ada kontribusi yang signifikan', 'Ada sejumlah kontribusi positif, namun tidak signifikan', 'Ada kontribusi positif dan cukup signifikan', 'Jelas terlihat adanya kontribusi yang positif dan signifikan serta dapat diukur besaran peningkatannya', 'Jelas terlihat adanya kontribusi yang positif dan signifikan, dapat diukur besaran peningkatannya dan memberikan manfaat langsung bagi para mahasiswa/i', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(45, 'Apakah ada kontribusi teknologi informasi dan komunikasi dalam hal peningkatan peran perguruan tinggi dalam konteks nasional dalam 5 taun terakhir ?', 3, 'Peningkatan kualitas', 'Tidak ada kontribusi yang signifikan', 'Ada kontribusi positif, namun tidak signifikan', 'Ada kontribusi positif dan cukup signifikan', 'Jelas terlihat adanya kontribusi yang positif dan signifikan serta dapat diukur besaran peningkatannya', 'Jelas terlihat adanya kontribusi yang positif dan signifikan, dapat diukur besaran peningkatannya dan memberikan manfaat langsung bagi pemerintah', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(46, 'Apakah pernah mendapatkan penghargaan dari lembaga-lembaga lain, dari luar maupun dalam negeri terkait dengan perkembangan teknologi informasi dan komunikasi di perguruan tinggi anda ?', 3, 'Peningkatan kualitas', 'Pernah, memperoleh 1 penghargaan dalam 10 tahun terakhir', 'Pernah, memperoleh 2-3 penghargaan dalam 10 tahun terakhir', 'Pernah, memperoleh 4-5 penghargaan dalam 10 tahun terakhir', 'Pernah, memperoleh 6-10 penghargaan dalam 10 tahun terakhir', 'Pernah, memperoleh lebih dari 10 penghargaan dalam 10 tahun terakhir', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(47, 'Apakah ada kontribusi nyata terkait dengan teknologi informasi dan komunikasi dalam hal peningkatan kualitas pengetahuan dan kompetensi mahasiswa/i di berbagai jurusan dalam 5 tahun terakhir ?', 3, 'Peningkatan kualitas', 'Tidak ada kontribusi yang signifikan', 'Ada sejumlah kontribusi positif, namun tidak signifikan', 'Ada kontribusi positif dan cukup signifikan', 'Jelas terlihat adanya kontribusi yang positif dan signifikan serta dapat diukur besaran peningkatannya', 'Jelas terlihat adanya kontribusi yang positif dan signifikan, dapat diukur besaran peningkatannya dan memberikan manfaat langsung bagi mahasiswa/i', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(48, 'Apakah ada kontribusi nyata terkait teknologi informasi dan komunikasi dalam hal peningkatan kualitas pengetahuan dan kompetensi mahasiswa/i dari berbagai jurusan di perguruan tingg anda dalam 5 tahun', 3, 'Peningkatan kualitas', 'Tidak ada kontribusi yang signifikan', 'Ada sejumlah kontribusi positif, namun tidak signifikan', 'Ada kontribusi positif dan cukup signifikan', 'Jelas terlihat adanya kontribusi yang positif dan signifikan serta dapat diukur besaran peningkatannya', 'Jelas terlihat adanya kontribusi yang positif dan signifikan, dapat diukur besaran peningkatannya dan memberikan manfaat langsung bagi mahasiswa/i', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(49, 'Apakah ada kontribusi nyata terkait teknologi informasi dan komunikasi dalam hal peningkatan, efisiensi operasional penyelenggaraan perguruan tinggi anda dalam 5 tahun terakhir ?', 3, 'Efektivitas dan efisiensi', 'Tidak ada kontribusi yang signifikan', 'Ada sejumlah kontribusi positif, namun tidak signifikan', 'Ada kontribusi positif dan cukup signifikan', 'Jelas terlihat adanya kontribusi yang positif dan signifikan serta dapat diukur besaran peningkatannya', 'Jelas terlihat adanya kontribusi yang positif dan signifikan, dapat diukur besaran peningkatannya dan memberikan manfaat langsung bagi perguruan tinggi', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(50, 'Apakah ada kontribusi nyata terkait teknologi informasi dan komunikasi dalam hal peningkatan pendapatan perguruan tinggi dalam 5 tahun terakhir ?', 3, 'Efektivitas dan efisiensi', 'Tidak ada kontribusi yang signifikan', 'Ada sejumlah kontribusi positif, namun tidak signifikan', 'Ada kontribusi positif dan cukup signifikan', 'Jelas terlihat adanya kontribusi yang positif dan signifikan serta dapat diukur besaran peningkatannya', 'Jelas terlihat adanya kontribusi yang positif dan signifikan, dapat diukur besaran peningkatannya dan memberikan manfaat langsung bagi perguruan tinggi', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(51, 'Apakah ada kontribusi nyata terkait teknologi informasi dan komunikasi dalam hal perbaikan pegawai perguruan tinggi yang bersih, akuntabel dan transparan dalam 5 tahun terakhir ?', 3, 'Transparansi manajemen', 'Tidak ada kontribusi yang signifikan', 'Ada sejumlah kontribusi positif, namun tidak signifikan', 'Ada kontribusi positif dan cukup signifikan', 'Jelas terlihat adanya kontribusi yang positif dan signifikan serta dapat diukur besaran peningkatannya', 'Jelas terlihat adanya kontribusi yang positif dan signifikan, dapat diukur besaran peningkatannya dan memberikan manfaat langsung bagi manajemen', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(52, 'Apakah ada kontribusi nyata terkait teknologi informasi dan komunikasi dalam hal peningkatan optimalisasi pemanfaatan berbagai sumber daya yang dimiliki perguruan tinggi anda dalam 5 tahun terakhir ?', 3, 'Utilitas sumber daya', 'Tidak ada kontribusi yang signifikan', 'Ada sejumlah kontribusi positif, namun tidak signifikan', 'Ada kontribusi positif dan cukup signifikan', 'Jelas terlihat adanya kontribusi yang positif dan signifikan serta dapat diukur besaran peningkatannya', 'Jelas terlihat adanya kontribusi yang positif dan signifikan, dapat diukur besaran peningkatannya dan memberikan manfaat langsung bagi perguruan tinggi', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(53, 'Menurut pendapat anda, apa peranan dan target utama penerapan teknologi informasi dan komunikasi di perguruan tinggi anda ?', 3, 'Transformasi organisasi', 'Memfasilitas proses komunikasi dan diseminasi informasi agar lebih cepat', 'Memfasilitas proses komunikasi dan diseminasi informasi agar lebih cepat serta meningkatkan kerja operasional pelayanan', 'Memfasilitas proses komunikasi dan diseminasi informasi agar lebih cepat serta meningkatkan kerja operasional pelayanan dan membantu proses pengambilan keputusan yang efektif', 'Memfasilitas proses komunikasi dan diseminasi informasi agar lebih cepat, meningkatkan kerja operasional pelayanan, membantu proses pengambilan keputusan yang efektif dan memastikan terjadinya proses ', 'Memfasilitas proses komunikasi dan diseminasi informasi agar lebih cepat, meningkatkan kerja operasional pelayanan, membantu proses pengambilan keputusan yang efektif dan memastikan terjadinya proses ', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(54, 'Apakah kebijakan perguruan tinggi memperbolehkan penyelenggaraan mata kuliah berbasis e-learing ?', 4, 'Implementasi e-learning', 'Tidak diperbolehkan sama sekali', 'Diperbolehkan, namun tetap harus dicampur denga model pembelajaran klasik (tatap muka)', 'Diperbolehkan sesuai dengan peraturan Kemdiknas / Dikti yang ada', 'Diperbolehkan bahkan dianjurkan untuk dilaksanakan oleh segenap dosen pengajar', 'Diperbolehkan bahkan  dianjurkan untuk sejumlah dosen melaksanakan sesuai dengan pengembangan yang direncanakan', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(55, 'Apakah pimpinan memberikan himbauan akan pentingnya antara dosen pengajar saling berbagi pakai (sharing) materi perkuliahan dan pengetahuan yang dimilikinya ?', 4, 'Berbagai sumber daya', 'Tidak diberikan himbauan', 'Diberikan himbauan secara informal', 'Secara resmi pimpinan senantiasa memberikan himbauan tersebut dalam berbagai kesempatan formal', 'Terdapat surat keputusan dari pihak pimpinan yang mengharuskan para dosen untuk saling berbagi pengetahuan dan materi ajar yang dimilikinya', 'Terdapat surat keputusan dari pihak pimpinan yang mengharuskan para dosen untuk saling berbagi pengetahuan dan materi ajar yang dimilikinya dan dijadikan alat ukur kinerja dosen', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(56, 'Apakah perguruan tinggi anda menyediakan fasilitas teknologi untuk menunjang proses berbagi materi ajar antar dosen?', 4, 'Berbagai sumber daya', 'Ya, namun jarang sekali dipergunakan oleh dosen', 'Ya, namun hanya beberapa dosen saja yang aktif berbagi materi ajar', 'Ya dan rata-rata dosen saling berbagi materi ajar antar sesamanya', 'Ya dan kira-kira lebih dari 50% telah secara aktif saling berbagi materi ahar', 'Ya hampir semua dosen saling aktif berbagi materi ajar diantara sesamanya', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(57, 'Berapa banyak yang mengunjungi website perguruan tinggi sehari-hari (per idetitas unik) ?', 4, 'Berbagai sumber daya', 'Mungkin kira-kira dibawah 100 hits per hari', 'Kira-kira antara 101 – 1000 hits per hari', 'Kira-kira antara 1001 – 10.000 hits per hari', 'Kira-kira antara 10.001 – 100.000 hits per hari', 'Diatas 100.000 hits per hari', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(58, 'Berdasarkan rekam jejak, fitur website perguruan tinggi apa yang paling banyak dimanfaatkan oleh pengunjung ?', 4, 'Berbagai sumber daya', 'Tidak jelas, karena tidak ada catatannya', 'Fitur navigasi umum untuk mencari informasi', 'Fitur navigasi umum untuk mencari informasi dan berinteraksi dengan pihak perguruan tinggi', 'Fitur “searching “ untuk mencari dan mengunduh dokumen / artikel penting yang dimiliki perguruan tinggi', 'Fitur aplikasi yang menungkinkan pengunjung untuk turut berpartisipasi dalam berbagai program perguruan tinggi berbasis virtual (e-learning, webcast, knowledge simulationm smart campus dan lain-lain)', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(59, 'Apakah pimpinan menganjurkan agar para dosen mengunggah berbagai hasil karyanya ke internet ?', 4, 'Pendidikan terbuka', 'Tidak menganjurkan sama sekali', 'Menganjurkan hanya untuk artikel tertentu saja', 'Menganjurkan untuk semua karya yang dimiliki dengan tetap memperhatikan unsur HAKI yang dimiliki', 'Mengharuskan para dosen untuk sebanyak mungkin meng-upload hasil karyanya ke internet', 'Mengharuskan para dosen untuk sebanyak mungkin meng-upload hasil karyanya ke internet dan dijadikan sebagai bagian dari penilaian kinerja dosen', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(60, 'Apakah pimpinan dan segenap dosen perguruan tinggi mengetahui akan adanya model HAKI bernama “creative common” ?', 4, 'Pendidikan terbuka', 'Tidak mengetahui sama sekali', 'Pernah mendengar, namun tidak tahu persis artinya', 'Tahu mengenai keberadaannya dan mengerti maksudnya', 'Tahu mengenai keberadaannya, mengerti maksudnya dan telah dipergunakan sejumlah dosen perguruan tinggi', 'Tahu mengenai keberadaannya, mengerti maksudnya dan secara masif telah dipergunakan sejumlah dosen perguruan tinggi', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(61, 'Berapa banyak materi perkuliahan dosen yang diberikan secara terbuka dan gratis kepada publik ', 4, 'Pendidikan terbuka', 'Tidak ada sama sekali', 'Dibawah 10% dari total mata kuliah', 'Kira-kira antara 11%-25% dari total mata kuliah', 'Kira-kira antara 26%-50% dari total mata kuliah', 'Diatas 50% dari total mata kuliah', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(62, 'Apakah perguruan tinggi menjadi salah satu penggiat atau peserta forum / asosiasi Open Course Ware (OCW)', 4, 'Pendidikan terbuka', 'Tidak ada sama sekali', 'Berencana untuk menjadi anggotanya dalam waktu dekat', 'Menjadi penggiat dan anggota aktif dari perkumpulan Open Course Ware (OCW)', 'Menjadi penggiat dan anggota aktif dan sekaligus pengurus Open Course Ware (OCW) di sejumlah tempat', 'Menjadi inisiator sekaligus pemimpin gerakan Open Course Ware (OCW) di sejumlah wilayah', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(63, 'Bagaimana model pengelolaan sistem informasi internal terkait dengan kewajiban melaporkan status kampus melalui sistem informasi PDPT (Pangkalan Data Perguruan Tinggi) ?', 4, 'Pangkalan data terpadu', 'Perguruan tinggi tidak memiliki sistem informasi internal', 'Keduanya merupakan sistem yang berdiri sendiri, tidak berhubungan', 'Sistem informasi dirancang sedemikian rupa sehingga menghasilkan output sebagaimana format file PDPT', 'Dikti dapat secara langsung mengambil file (mengunduh) dari hasil keluaran sistem informasi internal yang formatnya sesuai dengan PDPT melalui website perguruan tinggi', 'Kedua sistem sudah terintegrasi', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(64, 'Apakah sistem informasi akademik perguruan tinggi dapat diakses melalui website oleh pihak luar ?', 4, 'Pangkalan data terpadu', 'Tidak dapat sama sekali', 'Dapat diakses hanya untuk dosen atau mahasiswa aktif (alumni) perguruan tinggi', 'Dapat diakses oleh berbagai pihak sesuai dengan hak otoritasnya masing-masing', 'Dapat diakses oleh berbagai pihak yang membutuhkan data/informasi mengenai dosen, mahasiswa, mata kuliah, materi ajar, pustaka internal dan lain-lain, dimana sistem mencatat keseluruhan model interaks', 'Dapat diakses oleh berbagai pihak luar untuk mendapatkan informasi / data yang dibutuhkan disamping itu dapat dilakukan berbagai query atau searching terhadap kumpulan basis data yang dimiliki perguru', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(65, 'Apakah perguruan tinggi memiliki jejaring ke institusi pendidikan atau penelitian lain di luar negeri ?', 4, 'Jejaring internasional', 'Tidak ada', 'Sedang menjajaki kerjasama dengan pihak luar negeri', 'Memiliki jejaring ke sejumlah lembaga atau institusi lain di luar negeri', 'Memiliki jejaring ke sejumlah institusi lain di luar negeri yang memungkinkan dosen untuk saling mengakses sumber-sumber pembelajaran dari pihak mitra', 'Memiliki jejaring ke sejumlah institusi lain di luar negeri yang memungkinkan dosen dan mahasiswa untuk saling mengakses sumber-sumber pembelajaran dari pihak mitra', '2023-02-04 17:02:01', '2023-02-04 17:02:01'),
(66, 'Apakah perguruan tinggi anda mendapatkan dukungan dan bantuan sumber daya maupun asistensi dari pihak ketiga (lembaga internasional) yang berasal dari luar negeri ?', 4, 'Jejaring internasional', 'Tidak ada', 'Ada, namun sangat jarang sekali (satu dalam 3-5 tahun)', 'Ada, namun berupa bantuan kecil paling tidak setahun sekali', 'Ada, jumlah bantuannya cukup besar dan diterima secara berkesinambungan', 'Ada, jumlah bantuannya besar dan diterima secara kontinyu serta berkesinambungan', '2023-02-04 17:02:01', '2023-02-04 17:02:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbpengguna`
--

CREATE TABLE `tbpengguna` (
  `id` int(11) NOT NULL,
  `nama_pengguna` varchar(50) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `handphone` varchar(20) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `hak_akses` varchar(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbpengguna`
--

INSERT INTO `tbpengguna` (`id`, `nama_pengguna`, `username`, `password`, `handphone`, `alamat`, `email`, `hak_akses`, `created_at`, `updated_at`) VALUES
(1, 'My Administrator', 'admin', '$2y$10$TTPVllz8HxtrAPk.o6.uj.jHWg0MXr4cBp1g/IFKFbeC1ik6a/dJW', '1111', 'DKI Jakarta', 'admin@gmail.com', 'Administrator', '2023-02-03 19:42:13', '2023-02-03 19:42:13'),
(3, 'Alec Shiba', 'alec', '$2y$10$5dEwp7gwG/7.dqQGtjD0.u/rQLQuFL5kQ3jcFVYktsz.OEnP7W7xW', '081292929999', 'Jakarta Selatan, Indonesia', 'alec@gmail.com', 'Dekan', '2023-02-03 19:44:07', '2023-02-03 19:44:07'),
(4, 'Arief Hidayatullah', 'ariefh', '$2y$10$5/2KkQ0VYOQ.dQSblr/xiOweT5lyQOudlWgGY9E47jDgPsreFmj6y', '081384849090', 'Bandung, Jawa Barat', 'ariefh@gmail.com', 'Kaprodi SI', '2023-02-05 13:16:14', '2023-02-05 13:16:14'),
(5, 'Mohammed Faiz', 'faizm', '$2y$10$KMnctP0mM2YesRo.D12U3.fL.HicjZQxqFVe0bpcGgZ3Y5hmdcGPy', '081384849091', 'Padang, Indonesia', 'faiz@gmail.com', 'Kaprodi SI', '2023-02-05 16:42:33', '2023-02-05 16:42:33');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbdimensi`
--
ALTER TABLE `tbdimensi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbjawaban`
--
ALTER TABLE `tbjawaban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kuesioner` (`id_kuesioner`),
  ADD KEY `fk_pengguna` (`id_pengguna`);

--
-- Indeks untuk tabel `tbkuesioner`
--
ALTER TABLE `tbkuesioner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbkuesioner_FK` (`id_dimensi`);

--
-- Indeks untuk tabel `tbpengguna`
--
ALTER TABLE `tbpengguna`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbdimensi`
--
ALTER TABLE `tbdimensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbjawaban`
--
ALTER TABLE `tbjawaban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=463;

--
-- AUTO_INCREMENT untuk tabel `tbkuesioner`
--
ALTER TABLE `tbkuesioner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT untuk tabel `tbpengguna`
--
ALTER TABLE `tbpengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbjawaban`
--
ALTER TABLE `tbjawaban`
  ADD CONSTRAINT `fk_kuesioner` FOREIGN KEY (`id_kuesioner`) REFERENCES `tbkuesioner` (`id`),
  ADD CONSTRAINT `fk_pengguna` FOREIGN KEY (`id_pengguna`) REFERENCES `tbpengguna` (`id`);

--
-- Ketidakleluasaan untuk tabel `tbkuesioner`
--
ALTER TABLE `tbkuesioner`
  ADD CONSTRAINT `tbkuesioner_FK` FOREIGN KEY (`id_dimensi`) REFERENCES `tbdimensi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
