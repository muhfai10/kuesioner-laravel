<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kuesioner;
use App\Models\Dimensi;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DB::table('tbkuesioner')
        ->join('tbdimensi', 'tbkuesioner.id_dimensi', '=', 'tbdimensi.id')
        ->select('tbkuesioner.id', 'tbkuesioner.pertanyaan', 'tbkuesioner.pila', 'tbdimensi.dimensi',
         'tbkuesioner.pilb', 'tbkuesioner.pilc', 'tbkuesioner.pild', 'tbkuesioner.pile', 'tbkuesioner.variabel')
        ->get();
        return view('pertanyaan.pertanyaan', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Kuesioner;
        $dimensis = Dimensi::all();
        return view('pertanyaan.create', compact('model','dimensis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['pertanyaan' => 'required',
        'id_dimensi' => 'required',
        'pila' => 'required',
        'pilb' => 'required',
        'pilc' => 'required',
        'pild' => 'required',
        'pile' => 'required']);

        $model = new Kuesioner;
        $model -> pertanyaan = $request->pertanyaan;
        $model -> id_dimensi = $request->id_dimensi;
        $model -> variabel = $request->variabel;
        $model -> pila = $request->pila;
        $model -> pilb = $request->pilb;
        $model -> pilc = $request->pilc;
        $model -> pild = $request->pild;
        $model -> pile = $request->pile;
        $model->save();

        if($model){
            return redirect()
                ->route('pertanyaan.index')
                ->with([
                    'success' => 'Berhasil Tambahkan Data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal Tambahkan Data'
                ]);
        }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Kuesioner::findOrFail($id);
        $dimensis = Dimensi::all();
        return view('pertanyaan.edit', compact('model','dimensis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['pertanyaan' => 'required',
        'id_dimensi' => 'required',
        'pila' => 'required',
        'pilb' => 'required',
        'pilc' => 'required',
        'pild' => 'required',
        'pile' => 'required']);

        $model = Kuesioner::find($id);
        $model -> pertanyaan = $request->pertanyaan;
        $model -> id_dimensi = $request->id_dimensi;
        $model -> variabel = $request->variabel;
        $model -> pila = $request->pila;
        $model -> pilb = $request->pilb;
        $model -> pilc = $request->pilc;
        $model -> pild = $request->pild;
        $model -> pile = $request->pile;
        $model->save();

        if($model){
            return redirect()
                ->route('pertanyaan.index')
                ->with([
                    'success' => 'Berhasil Ubah data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal Ubah data'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Kuesioner::findOrFail($id);
        $model->delete();
        if($model){
            return redirect()
                ->route('pertanyaan.index')
                ->with([
                    'success' => 'Berhasil Hapus Data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal Hapus Data'
                ]);
        }
    }
}
