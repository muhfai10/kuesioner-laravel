<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kuesioner;
use App\Models\Jawaban;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class KuesionerController extends Controller
{
    
    public function getAllKuesioner()
    {
        $exists = false;
        $datas =  DB::table('tbkuesioner')
        ->join('tbdimensi', 'tbkuesioner.id_dimensi', '=', 'tbdimensi.id')
        ->select('tbkuesioner.id', 'tbkuesioner.pertanyaan', 'tbkuesioner.pila', 'tbdimensi.dimensi',
         'tbkuesioner.pilb', 'tbkuesioner.pilc', 'tbkuesioner.pild', 'tbkuesioner.pile', 'tbkuesioner.variabel')
        ->get();
        //Kuesioner::all();//DB::table('tbkuesioner');
        return view('kuesioner.create', compact('datas', 'exists'));
    }

    public function store(Request $request)
    {
        $id_pengguna = Auth::user()->id;
        $allParam = $request->except('id_pengguna', '_token');
        $model = null;
        foreach($allParam as $key => $value){
            $model = new Jawaban;
            $id_kuesioner = Str::substr($key, 7);
            $model -> id_kuesioner = $id_kuesioner;
            $model -> id_pengguna = $id_pengguna;
            $model -> jawaban = $value;
            $model->save();
        }

        if($model){
            return redirect()
                ->route('dashboard.dashboard')
                ->with([
                    'success' => 'Berhasil Input Kuesioner'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal Input Kuesioner'
                ]);
        }
    }
}
