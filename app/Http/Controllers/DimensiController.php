<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dimensi;

class DimensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Dimensi::all();
        return view('dimensi.dimensi', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Dimensi;
        return view('dimensi.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['dimensi' => 'required',
    'bobot' => 'required']);
    $model = new Dimensi;
    $model -> dimensi = $request->dimensi;
    $model -> bobot = $request->bobot;
    $model->save();

    if($model){
        return redirect()
            ->route('dimensi.index')
            ->with([
                'success' => 'Berhasil Tambahkan Data'
            ]);
    }else{
        return redirect()
            ->back
            ->withInput()
            ->with([
                'error' => 'Gagal Tambahkan Data'
            ]);
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Dimensi::findOrFail($id);
        return view('dimensi.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['dimensi' => 'required',
        'bobot' => 'required']);
        $model = Dimensi::find($id);
        $model -> dimensi = $request->dimensi;
        $model -> bobot = $request->bobot;
        $model->save();

        if($model){
            return redirect()
                ->route('dimensi.index')
                ->with([
                    'success' => 'Berhasil Ubah Data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal Ubah Data'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Dimensi::findOrFail($id);
        $model->delete();
        if($model){
            return redirect()
                ->route('dimensi.index')
                ->with([
                    'success' => 'Berhasil Hapus Data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal Hapus Data'
                ]);
        }
    }
}
