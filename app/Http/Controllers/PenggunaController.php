<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengguna;

class PenggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Pengguna::all();
        return view('pengguna.pengguna', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Pengguna;
        return view('pengguna.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['nama_pengguna' => 'required',
    'username' => 'required',
    'password' => 'required',
    'hak_akses' => 'required']);

    $model = new Pengguna;
    $model -> nama_pengguna = $request->nama_pengguna;
    $model -> username = $request->username;
    $model -> email = $request->email;
    $model -> password = bcrypt($request->password);
    $model -> handphone = $request->handphone;
    $model -> alamat = $request->alamat;
    $model -> hak_akses = $request->hak_akses;
    $model->save();

    if($model){
        return redirect()
            ->route('pengguna.index')
            ->with([
                'success' => 'Berhasil Tambahkan Data'
            ]);
    }else{
        return redirect()
            ->back
            ->withInput()
            ->with([
                'error' => 'Gagal Tambahkan Data'
            ]);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Pengguna::findOrFail($id);
        return view('pengguna.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['nama_pengguna' => 'required',
        'username' => 'required',
        'hak_akses' => 'required']);

        $model = Pengguna::find($id);
        $model -> nama_pengguna = $request->nama_pengguna;
        $model -> username = $request->username;
        $model -> email = $request->email;
        $model -> handphone = $request->handphone;
        $model -> alamat = $request->alamat;
        $model -> hak_akses = $request->hak_akses;
        $model->save();
        
        if($model){
            return redirect()
                ->route('pengguna.index')
                ->with([
                    'success' => 'Berhasil Ubah Data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal Ubah Data'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Pengguna::findOrFail($id);
        $model->delete();
        if($model){
            return redirect()
                ->route('pengguna.index')
                ->with([
                    'success' => 'Berhasil Hapus Data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal Hapus Data'
                ]);
        }
    }
}
