<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Dimensi;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.newindex', [
            'title' => 'Login'
        ]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if(Auth::attempt($credentials)){
            $request->session()->regenerate();
            return redirect()->intended('/dashboard');
        }
        return back()->with('error', 'Login Gagal!');
    }

    public function afterLogin()
    {
        $id_pengguna = Auth::user()->id;
        $jawaban = DB::table('tbjawaban')->where('id_pengguna', '=', $id_pengguna )->get();
        $exists = false;
        if(!$jawaban->isEmpty()){
            $exists = true;
        }

        //Data Grafik Corporate Contribution
        $ccjawabana = collect(DB::SELECT("SELECT 
        Count(jawaban)* 1 jumlah 
      FROM 
        tbjawaban, 
        tbkuesioner 
      WHERE 
        (
          tbkuesioner.id = tbjawaban.id_kuesioner
        ) 
        AND (tbkuesioner.id_dimensi = 1) 
        and tbjawaban.jawaban = 'A' 
      "))->first();

        $ccjawabanb = collect(DB::SELECT("SELECT 
        Count(jawaban)* 2 jumlah 
        FROM 
        tbjawaban, 
        tbkuesioner 
        WHERE 
        (
        tbkuesioner.id = tbjawaban.id_kuesioner
        ) 
        AND (tbkuesioner.id_dimensi = 1) 
        and tbjawaban.jawaban = 'B' 
        "))->first();

        $ccjawabanc = collect(DB::SELECT("SELECT 
        Count(jawaban)* 3 jumlah 
        FROM 
        tbjawaban, 
        tbkuesioner 
        WHERE 
        (
        tbkuesioner.id = tbjawaban.id_kuesioner
        ) 
        AND (tbkuesioner.id_dimensi = 1) 
        and tbjawaban.jawaban = 'C' 
        "))->first();

        $ccjawaband = collect(DB::SELECT("SELECT 
        Count(jawaban)* 4 jumlah 
        FROM 
        tbjawaban, 
        tbkuesioner 
        WHERE 
        (
        tbkuesioner.id = tbjawaban.id_kuesioner
        ) 
        AND (tbkuesioner.id_dimensi = 1) 
        and tbjawaban.jawaban = 'D' 
        "))->first();

        $ccjawabane = collect(DB::SELECT("SELECT 
        Count(jawaban)* 5 jumlah 
        FROM 
        tbjawaban, 
        tbkuesioner 
        WHERE 
        (
        tbkuesioner.id = tbjawaban.id_kuesioner
        ) 
        AND (tbkuesioner.id_dimensi = 1) 
        and tbjawaban.jawaban = 'E' 
        "))->first();

        $totalCC=$ccjawabana->jumlah+$ccjawabanb->jumlah+$ccjawabanc->jumlah+$ccjawaband->jumlah+$ccjawabane->jumlah;
        if (($totalCC>=27) and ($totalCC<=48.6)) {
            $kategoriCC="Sangat Buruk";
            $bobot_kinerjaCC=1;
        } elseif (($totalCC>=48.7) and ($totalCC<=70.2)) {
            $kategoriCC="Buruk";
            $bobot_kinerjaCC=2;
        } elseif (($totalCC>=70.3) and ($totalCC<=91.8)) {
            $kategoriCC="Cukup Baik";
            $bobot_kinerjaCC=3;
        } elseif (($totalCC>=91.9) and ($totalCC<=113.4)) {
            $kategoriCC="Baik";
            $bobot_kinerjaCC=4;
        } else {
            $kategoriCC="Sangat Baik";
            $bobot_kinerjaCC=5;
        }

        $dimensiCC = Dimensi::findOrFail(1);
        $KPICC=($bobot_kinerjaCC/$dimensiCC->bobot)*100;

        if (($KPICC>=1) and ($KPICC<=20)) {
            $kategori_kpiCC="Tidak Baik";
        } elseif (($KPICC>=21) and ($KPICC<=40)) {
            $kategori_kpiCC="Kurang";
        } elseif (($KPICC>=41) and ($KPICC<=60)) {
            $kategori_kpiCC="Cukup";
        } elseif (($KPICC>=61) and ($KPICC<=80)) {
            $kategori_kpiCC="Baik";
        } else {
            $KPICC = 100;
            $kategori_kpiCC="Sangat Baik";
        }
        $kesimpulanCC="Kesimpulan KPI adalah " . $kategori_kpiCC;	

        


         //Data Grafik Stakeholder Orientation
         $sojawabana = collect(DB::SELECT("SELECT 
         Count(jawaban)* 1 jumlah 
       FROM 
         tbjawaban, 
         tbkuesioner 
       WHERE 
         (
           tbkuesioner.id = tbjawaban.id_kuesioner
         ) 
         AND (tbkuesioner.id_dimensi = 2) 
         and tbjawaban.jawaban = 'A' 
       "))->first();
 
         $sojawabanb = collect(DB::SELECT("SELECT 
         Count(jawaban)* 2 jumlah 
         FROM 
         tbjawaban, 
         tbkuesioner 
         WHERE 
         (
         tbkuesioner.id = tbjawaban.id_kuesioner
         ) 
         AND (tbkuesioner.id_dimensi = 2) 
         and tbjawaban.jawaban = 'B' 
         "))->first();
 
         $sojawabanc = collect(DB::SELECT("SELECT 
         Count(jawaban)* 3 jumlah 
         FROM 
         tbjawaban, 
         tbkuesioner 
         WHERE 
         (
         tbkuesioner.id = tbjawaban.id_kuesioner
         ) 
         AND (tbkuesioner.id_dimensi = 2) 
         and tbjawaban.jawaban = 'C' 
         "))->first();
 
         $sojawaband = collect(DB::SELECT("SELECT 
         Count(jawaban)* 4 jumlah 
         FROM 
         tbjawaban, 
         tbkuesioner 
         WHERE 
         (
         tbkuesioner.id = tbjawaban.id_kuesioner
         ) 
         AND (tbkuesioner.id_dimensi = 2) 
         and tbjawaban.jawaban = 'D' 
         "))->first();
 
         $sojawabane = collect(DB::SELECT("SELECT 
         Count(jawaban)* 5 jumlah 
         FROM 
         tbjawaban, 
         tbkuesioner 
         WHERE 
         (
         tbkuesioner.id = tbjawaban.id_kuesioner
         ) 
         AND (tbkuesioner.id_dimensi = 2) 
         and tbjawaban.jawaban = 'E' 
         "))->first();

        $totalSO=$sojawabana->jumlah+$sojawabanb->jumlah+$sojawabanc->jumlah+$sojawaband->jumlah+$sojawabane->jumlah;
        if (($totalSO>=27) and ($totalSO<=48.6)) {
            $kategoriSO="Sangat Buruk";
            $bobot_kinerjaSO=1;
        } elseif (($totalSO>=48.7) and ($totalSO<=70.2)) {
            $kategoriSO="Buruk";
            $bobot_kinerjaSO=2;
        } elseif (($totalSO>=70.3) and ($totalSO<=91.8)) {
            $kategoriSO="Cukup Baik";
            $bobot_kinerjaSO=3;
        } elseif (($totalSO>=91.9) and ($totalSO<=113.4)) {
            $kategoriSO="Baik";
            $bobot_kinerjaSO=4;
        } else {
            $kategoriSO="Sangat Baik";
            $bobot_kinerjaSO=5;
        }

        $dimensiSO = Dimensi::findOrFail(2);
        $KPISO=($bobot_kinerjaSO/$dimensiSO->bobot)*100;

        if (($KPISO>=1) and ($KPISO<=20)) {
            $kategori_kpiSO="Tidak Baik";
        } elseif (($KPISO>=21) and ($KPISO<=40)) {
            $kategori_kpiSO="Kurang";
        } elseif (($KPISO>=41) and ($KPISO<=60)) {
            $kategori_kpiSO="Cukup";
        } elseif (($KPISO>=61) and ($KPISO<=80)) {
            $kategori_kpiSO="Baik";
        } else {
            $KPISO = 100;
            $kategori_kpiSO="Sangat Baik";
        }
        $kesimpulanSO="Kesimpulan KPI adalah " . $kategori_kpiSO;	

           //Data Grafik Operational Excellence
           $oejawabana = collect(DB::SELECT("SELECT 
           Count(jawaban)* 1 jumlah 
         FROM 
           tbjawaban, 
           tbkuesioner 
         WHERE 
           (
             tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 3) 
           and tbjawaban.jawaban = 'A' 
         "))->first();
   
           $oejawabanb = collect(DB::SELECT("SELECT 
           Count(jawaban)* 2 jumlah 
           FROM 
           tbjawaban, 
           tbkuesioner 
           WHERE 
           (
           tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 3) 
           and tbjawaban.jawaban = 'B' 
           "))->first();
   
           $oejawabanc = collect(DB::SELECT("SELECT 
           Count(jawaban)* 3 jumlah 
           FROM 
           tbjawaban, 
           tbkuesioner 
           WHERE 
           (
           tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 3) 
           and tbjawaban.jawaban = 'C' 
           "))->first();
   
           $oejawaband = collect(DB::SELECT("SELECT 
           Count(jawaban)* 4 jumlah 
           FROM 
           tbjawaban, 
           tbkuesioner 
           WHERE 
           (
           tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 3) 
           and tbjawaban.jawaban = 'D' 
           "))->first();
   
           $oejawabane = collect(DB::SELECT("SELECT 
           Count(jawaban)* 5 jumlah 
           FROM 
           tbjawaban, 
           tbkuesioner 
           WHERE 
           (
           tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 3) 
           and tbjawaban.jawaban = 'E' 
           "))->first();

        $totalOE=$oejawabana->jumlah+$oejawabanb->jumlah+$oejawabanc->jumlah+$oejawaband->jumlah+$oejawabane->jumlah;
        if (($totalOE>=27) and ($totalOE<=48.6)) {
            $kategoriOE="Sangat Buruk";
            $bobot_kinerjaOE=1;
        } elseif (($totalOE>=48.7) and ($totalOE<=70.2)) {
            $kategoriOE="Buruk";
            $bobot_kinerjaOE=2;
        } elseif (($totalOE>=70.3) and ($totalOE<=91.8)) {
            $kategoriOE="Cukup Baik";
            $bobot_kinerjaOE=3;
        } elseif (($totalOE>=91.9) and ($totalOE<=113.4)) {
            $kategoriOE="Baik";
            $bobot_kinerjaOE=4;
        } else {
            $kategoriOE="Sangat Baik";
            $bobot_kinerjaOE=5;
        }

        $dimensiOE = Dimensi::findOrFail(3);
        $KPIOE=($bobot_kinerjaOE/$dimensiOE->bobot)*100;

        if (($KPIOE>=1) and ($KPIOE<=20)) {
            $kategori_kpiOE="Tidak Baik";
        } elseif (($KPIOE>=21) and ($KPIOE<=40)) {
            $kategori_kpiOE="Kurang";
        } elseif (($KPIOE>=41) and ($KPIOE<=60)) {
            $kategori_kpiOE="Cukup";
        } elseif (($KPIOE>=61) and ($KPIOE<=80)) {
            $kategori_kpiOE="Baik";
        } else {
            $KPIOE = 100;
            $kategori_kpiOE="Sangat Baik";
        }
        $kesimpulanOE="Kesimpulan KPI adalah " . $kategori_kpiOE;	

           //Data Grafik Future Orientation
           $fojawabana = collect(DB::SELECT("SELECT 
           Count(jawaban)* 1 jumlah 
         FROM 
           tbjawaban, 
           tbkuesioner 
         WHERE 
           (
             tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 4) 
           and tbjawaban.jawaban = 'A' 
         "))->first();
   
           $fojawabanb = collect(DB::SELECT("SELECT 
           Count(jawaban)* 2 jumlah 
           FROM 
           tbjawaban, 
           tbkuesioner 
           WHERE 
           (
           tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 4) 
           and tbjawaban.jawaban = 'B' 
           "))->first();
   
           $fojawabanc = collect(DB::SELECT("SELECT 
           Count(jawaban)* 3 jumlah 
           FROM 
           tbjawaban, 
           tbkuesioner 
           WHERE 
           (
           tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 4) 
           and tbjawaban.jawaban = 'C' 
           "))->first();
   
           $fojawaband = collect(DB::SELECT("SELECT 
           Count(jawaban)* 4 jumlah 
           FROM 
           tbjawaban, 
           tbkuesioner 
           WHERE 
           (
           tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 4) 
           and tbjawaban.jawaban = 'D' 
           "))->first();
   
           $fojawabane = collect(DB::SELECT("SELECT 
           Count(jawaban)* 5 jumlah 
           FROM 
           tbjawaban, 
           tbkuesioner 
           WHERE 
           (
           tbkuesioner.id = tbjawaban.id_kuesioner
           ) 
           AND (tbkuesioner.id_dimensi = 4) 
           and tbjawaban.jawaban = 'E' 
           "))->first();

        $totalFO=$fojawabana->jumlah+$fojawabanb->jumlah+$fojawabanc->jumlah+$fojawaband->jumlah+$fojawabane->jumlah;
        if (($totalFO>=27) and ($totalFO<=48.6)) {
            $kategoriFO="Sangat Buruk";
            $bobot_kinerjaFO=1;
        } elseif (($totalFO>=48.7) and ($totalFO<=70.2)) {
            $kategoriFO="Buruk";
            $bobot_kinerjaFO=2;
        } elseif (($totalFO>=70.3) and ($totalFO<=91.8)) {
            $kategoriFO="Cukup Baik";
            $bobot_kinerjaFO=3;
        } elseif (($totalFO>=91.9) and ($totalFO<=113.4)) {
            $kategoriFO="Baik";
            $bobot_kinerjaFO=4;
        } else {
            $kategoriFO="Sangat Baik";
            $bobot_kinerjaFO=5;
        }

        $dimensiFO = Dimensi::findOrFail(4);
        $KPIFO=($bobot_kinerjaFO/$dimensiFO->bobot)*100;

        if (($KPIFO>=1) and ($KPIFO<=20)) {
            $kategori_kpiFO="Tidak Baik";
        } elseif (($KPIFO>=21) and ($KPIFO<=40)) {
            $kategori_kpiFO="Kurang";
        } elseif (($KPIFO>=41) and ($KPIFO<=60)) {
            $kategori_kpiFO="Cukup";
        } elseif (($KPIFO>=61) and ($KPIFO<=80)) {
            $kategori_kpiFO="Baik";
        } else {
            $KPIFO = 100;
            $kategori_kpiFO="Sangat Baik";
        }
        $kesimpulanFO="Kesimpulan KPI adalah " . $kategori_kpiFO;	



        return view('dashboard.dashboard', compact('exists', 'ccjawabana', 'ccjawabanb', 'ccjawabanc', 'ccjawaband'
        , 'ccjawabane', 'kesimpulanCC', 'KPICC','sojawabana', 'sojawabanb', 'sojawabanc', 'sojawaband', 'sojawabane','kesimpulanSO', 'KPISO',
        'oejawabana', 'oejawabanb','oejawabanc', 'oejawaband', 'oejawabane', 'kesimpulanOE', 'KPIOE', 'fojawabana', 'fojawabanb','fojawabanc', 
        'fojawaband', 'fojawabane','kesimpulanFO', 'KPIFO',));
    }

    public function logout()
    {
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/login');

    }
}
